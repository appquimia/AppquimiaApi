<?php
/**
 * Description of SponsorDB
 *
 * @author meza
 */
class SponsorDB extends EntityDB {
    protected $mysqli;
    const TABLE = 'sponsors';
    
    public function getById($id=0){
        $query = "SELECT * FROM sponsors WHERE id=$id;";
        $stmt = $this->mysqli->prepare($query);
//        $stmt->bind_param('i', $id);
        $stmt->execute();
        $result = $stmt->get_result();
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $stmt->close();
        return $entity;
    }
    
    public function getContactoByIdSponsor($id=1){
        $query = "SELECT c.apellidos, c.nombres, c.email, c.telefono, c.cargo "
                . "FROM contactos c "
                . "LEFT JOIN contactosxsponsors cs ON c.id = cs.idcontacto "
                . "WHERE cs.idsponsor = " . $id;
//        var_dump($query);
        $result = $this->mysqli->query($query);
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $result->close();
        return $entity;
    }
    
    public function getListBySponsors(){
        $query = "SELECT s.id, s.razonsocial, s.direcciones, s.codigocanjeo, 
                s.telefono, s.cuit, s.email AS mail, s.imagen, s.fecultmodif, s.descripcion
                FROM sponsors s ORDER BY s.razonsocial;";
//        var_dump($query);
        $result = $this->mysqli->query($query);
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $result->close();
        return $entity;
    }
    
     public function getNews($fecmodif){
        $query="SELECT s.id, s.razonsocial, s.direcciones, s.codigocanjeo, 
                s.telefono, s.cuit, s.email, s.imagen, s.descripcion, s.fecultmodif
            FROM sponsors s
            WHERE (SELECT COUNT(id) As cant FROM sponsors WHERE fecultmodif > '$fecmodif') > 0";
//        var_dump($query);
        $result = $this->mysqli->query($query);
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $result->close();
        return $entity;
    }
    
    // TODO: agregar los campos nuevos imagen y descripcion
    public function insert($razonsocial='', $direcciones='', $codigocanjeo='', 
            $telefono='', $cuit=-1, $email='', 
            $descripcion='', $imagen=''){
        if(!$this->checkRazonSocial($razonsocial)) {
            $query = "INSERT INTO sponsors 
                    (razonsocial, direcciones, codigocanjeo, 
                    telefono, cuit, email, 
                    descripcion, imagen, fecultmodif) 
                VALUES ('$razonsocial', '$direcciones', '$codigocanjeo',
                    '$telefono', $cuit, '$email', 
                    '$descripcion', '$imagen', NOW());";
//            var_dump($query);
            $stmt = $this->mysqli->prepare($query);
            
            $r = $stmt->execute();

            $stmt->close();
            $r = $this->mysqli->insert_id;
        }
        else
            $r = false;
        return $r;
    }
    
    // TODO: agregar los campos nuevos imagen y descripcion
    public function update($id=-1, 
            $razonsocial='', $direcciones='', $codigocanjeo='', 
            $telefono='', $cuit=-1, $email='',
            $descripcion='', $imagen='') {
        if($this->checkID($id)){
            $query = "UPDATE sponsors SET 
                    razonsocial='$razonsocial', direcciones='$direcciones', codigocanjeo='$codigocanjeo', 
                    telefono='$telefono', cuit=$cuit, email='$email', 
                    descripcion='$descripcion', imagen='$imagen', fecultmodif=NOW() 
                WHERE id = $id;";
//            var_dump($query);
            $stmt = $this->mysqli->prepare($query);
            $r = $stmt->execute(); 
            $stmt->close();
            return $r;
        }
        return false;
    }
    
     public function checkID($id){
        $stmt = $this->mysqli->prepare("SELECT * FROM " . self::TABLE 
                . " WHERE ID=?");
        $stmt->bind_param("i", $id);
        if($stmt->execute()){
            $stmt->store_result();    
            if ($stmt->num_rows == 1){                
                return true;
            }
        }        
        return false;
    }
    
     public function checkRazonSocial($razonsocial){
        $stmt = $this->mysqli->prepare("SELECT * FROM " . self::TABLE 
                . " WHERE razonsocial=?");
        $stmt->bind_param("s", $razonsocial);
        if($stmt->execute()){
            $stmt->store_result();    
            if ($stmt->num_rows >= 1){
                return true;
            }
        }        
        return false;
    }
    
    public function delete($id=0) {
        $stmt = $this->mysqli->prepare("DELETE FROM ". self::TABLE ." WHERE id = ?;");
        $stmt->bind_param('i', $id);
        $r = $stmt->execute(); 
        $stmt->close();
        return $r;
    }
}
