<?php
/**
 * Description of PremioEstadoDB
 *
 * @author meza
 */
class PremioEstadoDB extends EntityDB {
   protected $mysqli;
   const TABLE = 'premiosestados';
    
    public function getById($id=0){
        $stmt = $this->mysqli->prepare("SELECT * FROM " 
                . self::TABLE . " WHERE id=?;");
        $stmt->bind_param('i', $id);
        $stmt->execute();
        $result = $stmt->get_result();
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $stmt->close();
        return $entity;
    }
	
    public function getList(){
        $query = "SELECT * FROM ". self::TABLE;
//        var_dump($query);
        $result = $this->mysqli->query($query);
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $result->close();
        return $entity;
    }
	
    public function insert($estado=""){
        $stmt = $this->mysqli->prepare(
                "INSERT INTO " . self::TABLE . " (estado) "
                . "VALUES (?);");
        $stmt->bind_param('s', $estado);
        $r = $stmt->execute();

        $stmt->close();
        return $r;
    }
    public function delete($id=0) {
        $stmt = $this->mysqli->prepare("DELETE FROM ". self::TABLE ." WHERE id = ?;");
        $stmt->bind_param('i', $id);
        $r = $stmt->execute(); 
        $stmt->close();
        return $r;
    }	
    public function update($id=-1, $estado="") {
        if($this->checkID($id)){
            $stmt = $this->mysqli->prepare(
                    "UPDATE " . self::TABLE . " SET estado=? "
                    . "WHERE id = ?;");
            $stmt->bind_param('si', $estado,$id);
            $r = $stmt->execute(); 
            $stmt->close();
            return $r;
        }
        return false;
    }
	public function checkID($id){
        $stmt = $this->mysqli->prepare("SELECT * FROM " . self::TABLE 
                . " WHERE ID=?");
        $stmt->bind_param("i", $id);
        if($stmt->execute()){
            $stmt->store_result();    
            if ($stmt->num_rows == 1){                
                return true;
            }
        }        
        return false;
    }
}
