<?php
/**
 * Description of MovilAPI
 *
 * @author meza
 */
class MovilAPI extends EntityAPI {
    const API_ACTION = 'movil';
    const PREFIX_CONDUCTORESXMOVILES = 'conductoresxmoviles';
    const PREFIX_DETALLES = 'detalles';
    
    public function API(){
        header('Content-Type: application/JSON');                
        $method = $_SERVER['REQUEST_METHOD'];
		$this->db = new MovilDB();
		
        switch ($method) {
            case 'GET'://consulta
                $this->processGet();
                break;
            default://metodo NO soportado
                echo 'METODO NO SOPORTADO';
                break;
        }
    }
    
    function processGet() {
        $id = filter_input(INPUT_GET, 'id');
        if ($id) {
            $isConductorxMovil = isset($id) ? $id === self::PREFIX_CONDUCTORESXMOVILES : false;
            $isDetalle = isset($id) ? $id === self::PREFIX_DETALLES : false;

            if ($isConductorxMovil) {
                $id = filter_input(INPUT_GET, 'fld1');
                $response = $this->db->getConductoresByIdmoviles($id);
                echo json_encode($response, JSON_PRETTY_PRINT);
            }elseif ($isDetalle) {
                $id = filter_input(INPUT_GET, 'fld1');
                $response = $this->db->getDetalleById($id);
                echo json_encode($response, JSON_PRETTY_PRINT);
            }elseif ($id) {
                $response = $this->db->getById($id);
                echo json_encode($response, JSON_PRETTY_PRINT);
            } else {
                $this->response(400);
            }
        } else {
            $response = $this->db->getList();
            echo json_encode($response, JSON_PRETTY_PRINT);
        }
    }
}
