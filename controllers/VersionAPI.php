<?php
/**
 * Description of VersionAPI
 *
 * @author meza
 */
class VersionAPI extends EntityAPI {
    const API_ACTION = 'version';
    const GET_VERSION = 'version';

    public function __construct() {
        $this->db = new VersionDB();
        $this->fields = [];
        array_push($this->fields, 
                'version',
                'mantenimiento');
    }
    
    function processGet() {
        $response = $this->db->getVersion();
        echo json_encode($response, JSON_PRETTY_PRINT);
    }
    
    function processPost() {
        $obj = json_decode( file_get_contents('php://input') );
        $objArr = (array)$obj;
        
        if (empty($objArr)) {
            $this->response(422,"error","Nothing to add. Check json");
            exit;
        }
        
        if(!$this->checkFields($obj)) {
            $this->response(422,"error","The property is not defined");
            exit;
        }
        
        $r = $this->db->insert(
                $obj->version, $obj->mantenimiento);
        if($r) {$this->response(200,"success", $r); }
        else {$this->response(204,"error","No record added"); }
    }
    
    function processPut() {
        $obj = json_decode(file_get_contents('php://input') );
        if(!$this->checkFields($obj)) {
            $this->response(422,"error","The property is not defined");
            exit;
        }
        $id = filter_input(INPUT_GET, 'id');
        if(!$id) {
            $this->response(422,"error","Id no enviado.");
            exit;
        }
        $r = $this->db->update($id,
                $obj->version, $obj->mantenimiento);
        if($r) { $this->response(200,"success", $id); }
        else { $this->response(204,"success","Record not updated");}
    }
}