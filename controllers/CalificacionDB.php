<?php
/**
 * Description of CalificacionDB
 *
 * @author meza
 */
class CalificacionDB extends EntityDB {
   protected $mysqli;
   const TABLE = 'calificaciones';
    
    public function getById($id=0){
        $query = "SELECT * FROM " 
                . self::TABLE . " WHERE id=$id;";
        $stmt = $this->mysqli->prepare($query);
        $stmt->execute();
        $result = $stmt->get_result();
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $stmt->close();
        return $entity;
    }
    
    public function rangoFechas($fecmin, $fecmax, $estrellas){
        $fechamin = ($fecmin !== '') ? ("cal.fecha >= '" . $fecmin . "' ") : ( "1 = 1 ");
        $fechamax = (($fecmax !== '') ? ("cal.fecha <= '" . $fecmax . "' " ) : ( "1 = 1 "));
        $cantestre = ($estrellas > 0) ? ("(c.estrellasmovil = " . $estrellas . " OR c.estrellasconductor = " . $estrellas . ") ") : ("1 = 1 ");

        $query = "SELECT c.* FROM (" 
                . "SELECT "
                . "cal.idmovil, FLOOR(AVG(cal.estrellasmovil)) AS estrellasmovil, "
                . "cal.idconductor, FLOOR(AVG(cal.estrellasconductor)) AS estrellasconductor, "
                . "IFNULL(CONCAT(con.apellidos, ', ' , con.nombres), 'CONDUCTOR INEXISTENTE') AS conductor, "
                . "mov.nromovil "
                . "FROM calificaciones cal "
                . "LEFT JOIN conductoresxmoviles cxm ON cxm.idmovil = cal.idmovil AND cxm.idconductor = cal.idconductor "
                . "LEFT JOIN conductores con ON con.id = cxm.idconductor "
                . "LEFT JOIN moviles mov ON mov.id = cxm.idmovil "
                . "WHERE " . $fechamin . " AND " . $fechamax
                . "GROUP BY cal.idmovil, cal.idconductor) c "
                . "WHERE " . $cantestre;
       
        //var_dump($query);
        $result = $this->mysqli->query($query);
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $result->close();
        return $entity;
    }
    
    public function getResumen($idmovil, $idconductor){
        
        $query = "SELECT cal.idmovil, cal.idconductor, "
                . "CONCAT(con.apellidos, ', ' , con.nombres) AS conductor, "
                . "mov.nromovil, mov.dominio, mov.imagen, "
                . "IFNULL(SUM(CASE cal.estrellasmovil WHEN 1 THEN 1 END), 0) AS movil1, "
                . "IFNULL(SUM(CASE cal.estrellasmovil WHEN 2 THEN 1 END), 0) AS movil2, "
                . "IFNULL(SUM(CASE cal.estrellasmovil WHEN 3 THEN 1 END), 0) AS movil3, "
                . "IFNULL(SUM(CASE cal.estrellasmovil WHEN 4 THEN 1 END), 0) AS movil4, "
                . "IFNULL(SUM(CASE cal.estrellasmovil WHEN 5 THEN 1 END), 0) AS movil5, "
                . "IFNULL(SUM(CASE cal.estrellasconductor WHEN 1 THEN 1 END), 0) AS conductor1, "
                . "IFNULL(SUM(CASE cal.estrellasconductor WHEN 2 THEN 1 END), 0) AS conductor2, "
                . "IFNULL(SUM(CASE cal.estrellasconductor WHEN 3 THEN 1 END), 0) AS conductor3, "
                . "IFNULL(SUM(CASE cal.estrellasconductor WHEN 4 THEN 1 END), 0) AS conductor4, "
                . "IFNULL(SUM(CASE cal.estrellasconductor WHEN 5 THEN 1 END), 0) AS conductor5, "
                . "CONCAT(res.apellidos, ', ' , res.nombres) AS responsable, res.telefono "
                . "FROM calificaciones cal "
                . "LEFT JOIN conductoresxmoviles cxm ON cxm.idmovil = cal.idmovil AND cxm.idconductor = cal.idconductor "
                . "LEFT JOIN moviles mov ON mov.id = cxm.idmovil "
                . "LEFT JOIN conductores con ON con.id = cxm.idconductor "
                . "LEFT JOIN responsables res ON res.id = mov.idresponsable "
                . "WHERE cal.idmovil = '$idmovil' AND cal.idconductor = '$idconductor' "
                . "GROUP BY cal.idmovil, cal.idconductor ";
       
        //var_dump($query);
        $result = $this->mysqli->query($query);
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $result->close();
        return $entity;
    }
    
    /*
     * Sólo se usa desde app móvil
     */
	 public function insert(
		$id='', $idcuenta='', $idbitacora='',
                $fecha='', $tipoappquimia=0, $estrellasconductor=0,
                $estrellasmovil=0, $idmovil='', $idconductor='',
                $idsponsor=0, $idlocal='', $estrellasatencion=0,
                $estrellaslocal=0){
        $id = self::gen_uuid();
        $query = "INSERT INTO calificaciones
                (id, idcuenta, idbitacora,
                fecha, tipoappquimia, estrellasconductor,
                estrellasmovil, idmovil, idconductor,
                idsponsor, idlocal, estrellasatencion,
                estrellaslocal)
            VALUES
                ('$id', '$idcuenta', '$idbitacora',
                '$fecha', $tipoappquimia, $estrellasconductor,
                $estrellasmovil, '$idmovil', '$idconductor',
                $idsponsor, '$idlocal', $estrellasatencion,
                $estrellaslocal);";
//        var_dump($query);
//        return true;
        $stmt = $this->mysqli->prepare($query);
        $r = $stmt->execute();
        $stmt->close();
        return $r;
    }
	/*
    public function insert($id= '', $idcuenta='', $estrellasconductor=-1, 
        $fecha='', $estrellasmovil=-1, $idmovil='', $idbitacora='', $idconductor=''){
        $stmt = $this->mysqli->prepare(
                "INSERT INTO " . self::TABLE . 
                " (id, idcuenta, estrellasconductor, fecha, estrellasmovil, "
                . "idmovil, idbitacora, idconductor) "
                . "VALUES (?, ?, ?, ?, ?, ?, ?, ?);");
        $stmt->bind_param('ssisisss', $id, $idcuenta, $estrellasconductor, $fecha, $estrellasmovil, 
                $idmovil, $idbitacora, $idconductor);
        $r = $stmt->execute();
        
        $stmt->close();
        return $r;
    }*/
    
    public function update($id=-1, $idcuenta='', $estrellasconductor='', $fecha='', 
            $estrellasmovil='', $idmovil=-1, $idbitacora='', $idconductor =-1) {
        if($this->checkIntID(self::TABLE, $id)){
            $stmt = $this->mysqli->prepare(
                    "UPDATE " . self::TABLE . " SET idcuenta=?, estrellasconductor=?, "
                    . "fecha=?, estrellasmovil=?, idmovil=?, idbitacora=? "
                    . "WHERE id = ?;");
            $stmt->bind_param('sisiisii', $idcuenta, $estrellasconductor, $fecha, 
                    $estrellasmovil, $idmovil, $idbitacora, $id, $idconductor);
            $r = $stmt->execute(); 
            $stmt->close();
            return $r;
        }
        return false;
    }
    
    public function delete($id=0) {
        $stmt = $this->mysqli->prepare("DELETE FROM ". self::TABLE ." WHERE id = ?;");
        $stmt->bind_param('i', $id);
        $r = $stmt->execute(); 
        $stmt->close();
        return $r;
    }
}
