<?php
/**
 * Description of TarifaAPI
 *
 * @author meza
 */
class TarifaPlantillaAPI extends EntityAPI {
    const API_ACTION = 'tarifaplantilla';
    const GET_TARIFAS = 'tarifas';
    const GET_FIJAS = 'fijas';
    const GET_ESPERA = 'espera';
    const PUT_TARIFAS = 'tarifas';
    const PUT_FIJAS= 'fijas';
    const PUT_ESPERA = 'espera';
    const PUT_APLICAR = 'aplicar';

    public function __construct() {
        $this->db = new TarifaPlantillaDB();
    }
    
    function processGet(){
        $id = filter_input(INPUT_GET, 'id');
        $isTarifas = isset($id) ? $id === self::GET_TARIFAS : false;
        $isFijas = isset($id) ? $id === self::GET_FIJAS : false;
        $isEspera = isset($id) ? $id === self::GET_ESPERA : false;
        if ($isTarifas) {
            $response = $this->db->getTarifas();
            echo json_encode($response, JSON_PRETTY_PRINT);
        } elseif ($isFijas)  {
            $response = $this->db->getFijas();
            echo json_encode($response, JSON_PRETTY_PRINT);
        } elseif ($isEspera)  {
            $response = $this->db->getEspera();
            echo json_encode($response, JSON_PRETTY_PRINT);
        }
    }
    
    function processPost() {
        $id = filter_input(INPUT_GET, 'id');
        $isTarifas = isset($id) ? $id === self::GET_TARIFAS : false;
        $isFijas = isset($id) ? $id === self::GET_FIJAS : false;
        $isEspera = isset($id) ? $id === self::GET_ESPERA : false;
        // No aplicable
    }
    
    function processPut() {
        $id = filter_input(INPUT_GET, 'id');
        if (!$id) {
            $this->response(400);
            exit;
        }
        $isTarifas = isset($id) ? $id === self::PUT_TARIFAS : false;
        $isFijas = isset($id) ? $id === self::PUT_FIJAS : false;
        $isEspera = isset($id) ? $id === self::PUT_ESPERA : false;
        $isAplicar = isset($id) ? $id === self::PUT_APLICAR : false;
        
        $obj = json_decode(file_get_contents('php://input'));
        $objArr = (array) $obj;
        if (empty($objArr)) {
            $this->response(421, "error", "Nothing to add. Check json");
            exit;
        }
        if ($isTarifas) {
            if (isset($obj->id) AND isset($obj->valor)) {
                if ($this->db->updateTarifa($obj->id, $obj->valor)) {
                    $this->response(200, "success", "Record updated");
                } else {
                    $this->response(304, "success", "Record not updated");
                }
            }else {
                $this->response(422, "error", "The property is not defined");
            }
        } elseif ($isFijas) {
            if (isset($obj->id) AND isset($obj->precio)) {
                if ($this->db->updateFija($obj->id, $obj->precio)) {
                    $this->response(200, "success", "Record updated");
                } else{
                    $this->response(304, "success", "Record not updated");
                }
            }else {
                $this->response(422, "error", "The property is not defined");
            }
        } elseif ($isEspera) {
            if (isset($obj->id) AND isset($obj->costo)) {
                if ($this->db->updateEspera($obj->id, $obj->costo)) {
                    $this->response(200, "success", "Record updated");
                } else{
                    $this->response(304, "success", "Record not updated");
                }
            }else {
                $this->response(422, "error", "The property is not defined");
            }
        } elseif ($isAplicar) {
            if ($this->db->aplicar()) {
                $this->response(200, "success", "Record updated");
            } else{
                $this->response(304, "success", "Record not updated");
            }
        } else {
            $this->response(422, "error", "The method not recognized");
        }
    }
}
