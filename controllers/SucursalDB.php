<?php
/**
 * Description of SucursalDB
 *
 * @author meza
 */
class SucursalDB extends EntityDB {
    protected $mysqli;
    const TABLE = 'sucursales';
    
    public function getById($id=''){
        $query = "SELECT * FROM sucursales WHERE id='$id';";
        $stmt = $this->mysqli->prepare($query);
        $stmt->execute();
        $result = $stmt->get_result();
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $stmt->close();
        return $entity;
    }
    
    public function getByIdSponsor($id=1){
        $query = "SELECT s.id, s.idsponsor, s.sucursal, s.direccion, s.descripcion,
                (FLOOR(IFNULL((
                SUM(CASE c.estrellasatencion WHEN 5 THEN 1 ELSE 0 END) * 5 +
                SUM(CASE c.estrellasatencion WHEN 4 THEN 1 ELSE 0 END) * 4 +
                SUM(CASE c.estrellasatencion WHEN 3 THEN 1 ELSE 0 END) * 3 +
                SUM(CASE c.estrellasatencion WHEN 2 THEN 1 ELSE 0 END) * 2 +
                SUM(CASE c.estrellasatencion WHEN 1 THEN 1 ELSE 0 END) * 1) / COUNT(IFNULL(c.id, 1)), 0))) estrellasatencion,
                (FLOOR(IFNULL((
                SUM(CASE c.estrellaslocal WHEN 5 THEN 1 ELSE 0 END) * 5 +
                SUM(CASE c.estrellaslocal WHEN 4 THEN 1 ELSE 0 END) * 4 +
                SUM(CASE c.estrellaslocal WHEN 3 THEN 1 ELSE 0 END) * 3 +
                SUM(CASE c.estrellaslocal WHEN 2 THEN 1 ELSE 0 END) * 2 +
                SUM(CASE c.estrellaslocal WHEN 1 THEN 1 ELSE 0 END) * 1) / COUNT(IFNULL(c.id, 1)), 0))) estrellaslocal
            FROM sucursales s 
            LEFT JOIN calificaciones c ON c.idlocal = s.id
            WHERE s.idsponsor = $id
            GROUP BY id";
//        var_dump($query);
        $result = $this->mysqli->query($query);
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $result->close();
        return $entity;
    }
    
     public function getNews($fecmodif){
        $query="SELECT s.id, s.idsponsor, s.sucursal, s.direccion, s.descripcion
                FROM sucursales s
                LEFT JOIN sponsors sp ON sp.id = s.idsponsor
                WHERE (SELECT COUNT(id) As cant FROM sponsors WHERE fecultmodif > '$fecmodif') > 0";
//        var_dump($query);
        $result = $this->mysqli->query($query);
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $result->close();
        return $entity;
    }
    
    public function getList() {
         $query="SELECT s.id, s.idsponsor, s.sucursal, s.direccion, s.descripcion
                FROM sucursales s
                LEFT JOIN sponsors sp ON sp.id = s.idsponsor";
//        var_dump($query);
        $result = $this->mysqli->query($query);
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $result->close();
        return $entity;
    }
    
    public function insert(
        $idsponsor=-1, $sucursal='', $direccion='', 
        $descripcion=''){
        $id = $this->gen_uuid();
        $query = "INSERT INTO sucursales 
                (id, idsponsor, sucursal, direccion,
                descripcion, fecultmodif) 
            VALUES ('$id', $idsponsor, '$sucursal', '$direccion', 
                '$descripcion', NOW());";
//            var_dump($query);
//            return true;
        $stmt = $this->mysqli->prepare($query);

        $r = $stmt->execute();
        $stmt->close();
        return ($r) ? ($id) : (false);
    }
    
    // TODO: agregar los campos nuevos imagen y descripcion
    public function update($id='', 
            $idsponsor=-1, $sucursal='', $direccion='', 
            $descripcion='') {
        $query = "UPDATE sucursales SET 
                idsponsor=$idsponsor, sucursal='$sucursal', direccion='$direccion', 
                descripcion='$descripcion', fecultmodif=NOW() 
            WHERE id = '$id';";
        if($this->checkStringID(self::TABLE, $id)){
            $stmt = $this->mysqli->prepare($query);
            $r = $stmt->execute(); 
            $stmt->close();
            return $r;
        }
        return false;
    }
    
    public function delete($id='') {
        $query = "DELETE FROM ". self::TABLE ." WHERE id = '$id';";
//        var_dump($query);
//        return true;
        $stmt = $this->mysqli->prepare($query);
        $r = $stmt->execute();
        $stmt->close();
        return $r;
    }
}
