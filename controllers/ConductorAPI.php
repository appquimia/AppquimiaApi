<?php
/**
 * Description of ConductorAPI
 *
 * @author meza
 */
class ConductorAPI extends EntityAPI {
    const API_ACTION = 'conductor';
    const PREFIX_MOVILES = 'm';
    
    public function API(){
        header('Content-Type: application/JSON');                
        $method = $_SERVER['REQUEST_METHOD'];
		$this->db = new ConductorDB();
		
        switch ($method) {
            case 'GET'://consulta
                $this->processGet();
                break;
            default://metodo NO soportado
                echo 'METODO NO SOPORTADO';
                break;
        }
    }
    
    function processGet() {
        if ($_GET['action'] == self::API_ACTION) {
            if (isset($_GET['id'])) {
                $id = $_GET['id'];
                $isMoviles = strpos($id, self::PREFIX_MOVILES);
                if ($isMoviles !== false) {
                    $response = $this->db->getListByIdMovil(substr($id, 1));
                    echo json_encode($response, JSON_PRETTY_PRINT);
                } else {
                    $response = $this->db->getById($id);
                    echo json_encode($response, JSON_PRETTY_PRINT);
                }
            } else{
                $response = $this->db->getList();
                echo json_encode($response, JSON_PRETTY_PRINT);
            }
        } else {
            $this->response(400);
        }
    }
  
    }