<?php
/**
 * Description of ConductoresDB
 *
 * @author meza
 */
class ConductorDB extends EntityDB {
   protected $mysqli;
   const TABLE = 'conductores';
    
    public function getById($id=0){
        $stmt = $this->mysqli->prepare("SELECT * FROM " 
                . self::TABLE . " WHERE id=?;");
        $stmt->bind_param('s', $id);
        $stmt->execute();
        $result = $stmt->get_result();
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $stmt->close();
        return $entity;
    }
    
    public function getListByIdMovil($idmovil){
        $query= "SELECT c.id, c.apellidos, c.nombres, c.imagen, null as imagenb, c.activo "
                . "FROM conductores c "
                . "LEFT JOIN conductoresxmoviles m ON c.id = m.idconductor "
                . "WHERE m.idmovil='$idmovil'";
        //var_dump($query);
        $result = $this->mysqli->query($query);
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $result->close();
        return $entity;
    }
    
    public function getList(){
        $result = $this->mysqli->query(
                "SELECT c.id, c.apellidos, c.nombres, c.imagen, null as imagenb, activo "
                . "FROM conductores c "
                . "WHERE activo = 1;");
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $result->close();
        return $entity;
    }
     public function checkID($id){
        $stmt = $this->mysqli->prepare("SELECT * FROM " . self::TABLE 
                . " WHERE ID=?");
        $stmt->bind_param("i", $id);
        if($stmt->execute()){
            $stmt->store_result();    
            if ($stmt->num_rows == 1){                
                return true;
            }
        }        
        return false;
    }
}