<?php
/**
 * Description of MovilDB
 *
 * @author meza
 */
class MovilDB extends EntityDB {
   protected $mysqli;
   const TABLE = 'moviles';
    
    public function getById($id=0){
        $stmt = $this->mysqli->prepare("SELECT * FROM " 
                . self::TABLE . " WHERE id=?;");
        $stmt->bind_param('i', $id);
        $stmt->execute();
        $result = $stmt->get_result();
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $stmt->close();
        return $entity;
    }
    
    public function getDetalleById($id=1){
        $query = "SELECT m.id, m.descripcion, m.dominio, m.idresponsable, "
                . "CONCAT(r.apellidos,', ', r.nombres) AS responsable, r.dni, "
                . "r.telefono, m.imagen, m.nromovil, c.estrellas1, c.estrellas2, "
                . "c.estrellas3, c.estrellas4, c.estrellas5, m.activo "
                . "FROM moviles m "
                . "LEFT JOIN responsables r ON m.idresponsable = r.id "
                . "LEFT JOIN ( SELECT idmovil, "
                . "IFNULL(CASE estrellasmovil WHEN 1 THEN COUNT(estrellasmovil) END,0) AS estrellas1, "
                . "IFNULL(CASE estrellasmovil WHEN 2 THEN COUNT(estrellasmovil) END,0) AS estrellas2, "
                . "IFNULL(CASE estrellasmovil WHEN 3 THEN COUNT(estrellasmovil) END,0) AS estrellas3, "
                . "IFNULL(CASE estrellasmovil WHEN 4 THEN COUNT(estrellasmovil) END,0) AS estrellas4, "
                . "IFNULL(CASE estrellasmovil WHEN 5 THEN COUNT(estrellasmovil) END,0) AS estrellas5 "
                . "FROM calificaciones GROUP BY idmovil) c ON m.id = c.idmovil "
                . "WHERE m.id =  ". $id;
        $result = $this->mysqli->query($query);
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $result->close();
        return $entity;
    }
    
    public function getList(){
        $query = "SELECT m.id, m.descripcion, m.dominio, m.idresponsable, m.imagen, m.nromovil, 
                IFNULL(s.bloqueaingreso, 0) AS bloqueaingreso, IFNULL(s.bloqueajuego, 0) AS bloqueajuego
            FROM moviles m
            LEFT JOIN movilessuspendidos s ON s.idmovil = m.id
            WHERE activo = 1";
        $result = $this->mysqli->query($query);
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $result->close();
        return $entity;
    }
    
        public function getConductoresByIdmoviles($id=1){
        $query = "SELECT c.apellidos, c.nombres, c.imagen "
                . "FROM conductores c "
                . "LEFT JOIN conductoresxmoviles cm ON c.id = cm.idconductor "
                . "WHERE cm.idmovil = " . $id;
        $result = $this->mysqli->query($query);
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $result->close();
        return $entity;
    }
    
   public function checkID($id){
        $stmt = $this->mysqli->prepare("SELECT * FROM " . self::TABLE 
                . " WHERE ID=?");
        $stmt->bind_param("i", $id);
        if($stmt->execute()){
            $stmt->store_result();    
            if ($stmt->num_rows == 1){                
                return true;
            }
        }        
        return false;
    }
}