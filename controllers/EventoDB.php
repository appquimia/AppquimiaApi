<?php
/**
 * Description of EventoDB
 *
 * @author meza
 */
class EventoDB extends EntityDB {
   protected $mysqli;
   const TABLE = 'eventos';
    
    public function getById($id=1){
        $stmt = $this->mysqli->prepare("SELECT * FROM " 
                . self::TABLE . " WHERE id=?;");
        $stmt->bind_param('i', $id);
        $stmt->execute();
        $result = $stmt->get_result();
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $stmt->close();
        return $entity;
    }
    
    public function getEventoByIdReclamo($id=''){
        $query = "SELECT e.id, e.idreclamo, "
                . "e.descripcion, e.idusuario, u.usuario, "
                . "e.idestadoreclamo, re.estado, e.fechayhora "
                . "FROM eventos e "
                . "LEFT JOIN reclamosestados re ON e.idestadoreclamo = re.id "
                . "LEFT JOIN usuarios u ON e.idusuario = u.id "
                . "WHERE e.idreclamo = '" . $id ."'";
        $result = $this->mysqli->query($query);
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $result->close();
        return $entity;
    }
    
    public function insert($idreclamo='', $descripcion='', 
        $idusuario=-1, $idestadoreclamo=-1){

        $stmt = $this->mysqli->prepare("SET "
                . "@p_idreclamo:= ?, @p_descripcion:= ?, @p_idusuario:= ?, "
                . "@p_idestadoreclamo:= ?");
        
        $stmt->bind_param('ssii', $idreclamo, $descripcion, $idusuario, $idestadoreclamo);
        $stmt->execute();
        
        $stmt = $this->mysqli->prepare("SET @o_newid:= -1");
//        var_dump($stmt);
        $stmt->execute();
        
        $r = $this->mysqli->query('CALL sp_nuevoevento('
                . '@p_idreclamo, @p_descripcion, @p_idusuario, '
                . '@p_idestadoreclamo, @o_newid);');
        
        $r = $this->mysqli->query('SELECT @o_newid as o_newid');
        $row = $r->fetch_assoc();

        return $row['o_newid'];
    }
    
    public function update($id=-1, $idreclamo=-1, $descripcion='', 
            $idusuario=-1, $fechayhora='', $idestadoreclamo=-1) {
        if($this->checkStringID(self::TABLE , $id)){
            $stmt = $this->mysqli->prepare(
                    "UPDATE " . self::TABLE . " SET idreclamo=?, descripcion=?, "
                    . "idusuario=?, fechayhora=?, idestadoreclamo=? "
                    . "WHERE id = ?;");
            $stmt->bind_param('ssisii', $idreclamo, $descripcion, $idusuario, $fechayhora, $idestadoreclamo, $id);
            $r = $stmt->execute(); 
            $stmt->close();
            return $r;
        }
        return false;
    }
    
    public function delete($id=0) {
        $stmt = $this->mysqli->prepare("DELETE FROM ". self::TABLE ." WHERE id = ?;");
        $stmt->bind_param('i', $id);
        $r = $stmt->execute(); 
        $stmt->close();
        return $r;
    }
}
