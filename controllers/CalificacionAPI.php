<?php
/**
 * Description of CalificacionAPI
 *
 * @author meza
 */
class CalificacionAPI extends EntityAPI {
    const API_ACTION = 'calificacion';
    const TIPOAPPQUMIA_MOVIL = 0;
    const TIPOAPPQUMIA_COMMERCE = 1;
    const TIPOAPPQUMIA_STREET = 2;
    const TIPOAPPQUMIA_UNKNOWN = 3;
    const PATCH_RANGO_FECHAS = 'f';    
    const PATCH_RESUMEN = 'r';

    public function __construct() {
        $this->db = new CalificacionDB();
        $this->fields = [];
        array_push($this->fields, 
            'id',
            'idcuenta',
            'estrellasconductor',
            'fecha',
            'estrellasmovil',
            'idmovil',
            'idbitacora',
            'idconductor',
            'idsponsor',
            'tipoappquimia',
            'idlocal',
            'estrellasatencion',
            'estrellaslocal');
    }

    function processGet() {
        if (isset($_GET['id'])) {
            $response = $this->db->getById($_GET['id']);
            echo json_encode($response, JSON_PRETTY_PRINT);
        }
    }

    function processPatch() {
        if ($_GET['action'] == self::API_ACTION) {
            $obj = json_decode(file_get_contents('php://input'));
            $objArr = (array) $obj;

            if (isset($_GET['id'])) {
                $id = $_GET['id'];
                $isRangoFecha = isset($id) ? $id === self::PATCH_RANGO_FECHAS : false; //substr($id, 0, strlen(self::PATCH_RANGO_FECHAS)) === self::PATCH_RANGO_FECHAS;
                $isResumen = isset($id) ? $id === self::PATCH_RESUMEN : false; //substr($id, 0, strlen(self::PATCH_RESUMEN)) === self::PATCH_RESUMEN;

                if ($isRangoFecha) {
                    if (isset($obj->fecmin, $obj->fecmax, $obj->estrellas)) {
                        $response = $this->db->rangoFechas($obj->fecmin, $obj->fecmax, $obj->estrellas);
                        echo json_encode($response, JSON_PRETTY_PRINT);
                    } else {
                        $this->response(400);
                    }
                } else if($isResumen) {
                    if (isset($obj->idmovil, $obj->idconductor)) {
                        $response = $this->db->getResumen($obj->idmovil, $obj->idconductor);
                        echo json_encode($response, JSON_PRETTY_PRINT);
                    } else {
                        $this->response(400);
                    }
                } else {
                    $this->response(400);
                }
            } else {
                $this->response(400);
            }
        }
    }

//    function processPost() {
//        $obj = json_decode( file_get_contents('php://input') );
//        $objArr = (array)$obj;
//        if (empty($objArr)) {
//            $this->response(422,"error","Nothing to add. Check json");
//            exit;
//        }
//        $ret = false;
//        switch ($obj->tipoappquimia) {
//            case self::TIPOAPPQUMIA_MOVIL:
//                $ret = $this->postMovil($obj);
//                break;
//            case self::TIPOAPPQUMIA_COMMERCE:
//                $ret = $this->postCommerce($obj);
//                break;
//            case self::TIPOAPPQUMIA_STREET:
//                $ret = $this->postStreet($obj);
//                break;
//            default:
//                $ret = $this->postMovil($obj);
//                break;
//        }
//        if($ret == -1) {
//            $this->response(422, "error", "The property is not defined");
//            exit;
//        }
//        if($ret) {
//            $this->response(200,"id",$obj->id);
//        } else {
//            $this->response(422,"error","Duplicado");
//        }
//    }

    function processPut() {
        if (isset($_GET['action']) && isset($_GET['id'])) {
            if ($_GET['action'] == self::API_ACTION) {
                $obj = json_decode(file_get_contents('php://input'));
                $objArr = (array) $obj;
                if (empty($objArr)) {
                    $this->response(422, "error", "Nothing to add. Check json");
                } else if (isset($obj->idcuenta) AND isset($obj->estrellasconudctor)
                        AND isset($obj->fecha) AND isset($obj->estrellasmovil)
                        AND isset($obj->idmovil) AND isset($obj->idbitacora)) {
                    if ($this->db->update($_GET['id'], $obj->idcuenta, $obj->estrellasconudctor, $obj->fecha, $obj->estrellasmovil, $obj->idmovil, $obj->idbitacora))
                        $this->response(200, "success", "Record updated");
                    else
                        $this->response(304, "success", "Record not updated");
                }else {
                    $this->response(422, "error", "The property is not defined");
                }
                exit;
            }
        }
        $this->response(400);
    }

    function processDelete() {
        if (isset($_GET['action']) && isset($_GET['id'])) {
            if ($_GET['action'] == self::API_ACTION) {
                $this->db->delete($_GET['id']);
                $this->response(204);
                exit;
            }
        }
        $this->response(400);
    }

    function processPost() {
        $ret = false;
        $obj = json_decode( file_get_contents('php://input') );
        if(!$this->checkFields($obj)) {
            $this->response(422,"error","The property is not defined.");
            exit;
        }
        $ret = $this->db->insert($obj->id, $obj->idcuenta, $obj->idbitacora,
                $obj->fecha, $obj->tipoappquimia, $obj->estrellasconductor,
                $obj->estrellasmovil, $obj->idmovil, $obj->idconductor,
                $obj->idsponsor, $obj->idlocal, $obj->estrellasatencion,
                $obj->estrellaslocal);
        if($ret) {
            $this->response(200,"id",$obj->id);
        } else {
            $this->response(422,"error","Duplicado");
        }
    }

    function postCommerce($obj) {
        $ret = false;
        if (isset($obj->id) AND isset($obj->idcuenta) 
                AND isset($obj->estrellasatencion) AND isset($obj->fecha) 
                AND isset($obj->estrellaslocal) AND isset($obj->idsponsor) 
                AND isset($obj->idbitacora) AND isset($obj->idlocal) ) {
            $ret = $this->db->insert($obj->id, $obj->idcuenta, 
                    $obj->estrellasatencion, $obj->fecha, 
                    $obj->estrellaslocal, $obj->idsponsor, 
                    $obj->idbitacora, $obj->idlocal);
        }
        return $ret;
    }
    
    function postStreet($obj) {
        return true;
    }
}