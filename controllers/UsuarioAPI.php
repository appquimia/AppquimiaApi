<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of UsuarioApi
 *
 * @author WebDev
 */
class UsuarioAPI extends EntityAPI {
    const API_ACTION = 'usuarios';
    
    public function API(){
        header('Content-Type: application/JSON');                
        $method = $_SERVER['REQUEST_METHOD'];
        switch ($method) {
            case 'GET'://consulta
                $this->getUsuarios();
                break;
            case 'POST'://inserta
                $this->processPost();//saveUsuario();
                break;
            case 'PUT'://actualiza
                $this->updateUsuario();
                break;
            case 'DELETE'://elimina
                $this->deleteUsuario();
                break;
            default://metodo NO soportado
                echo 'METODO NO SOPORTADO';
                break;
        }
    }
    
    function getUsuarios(){
        if($_GET['action']==self::API_ACTION){
            $db = new UsuarioDB();
            if(isset($_GET['id'])){//muestra 1 solo registro si es que existiera ID
                $response = $db->getUsuario($_GET['id']);
                echo json_encode($response,JSON_PRETTY_PRINT);
            }else{ //muestra todos los registros
                $response = $db->getUsuarios();
                echo json_encode($response,JSON_PRETTY_PRINT);
            }
        }else{
                $this->response(400);
        }
    }
    
    function updateUsuario() {
        if( isset($_GET['action']) && isset($_GET['id']) ){
            if($_GET['action']==self::API_ACTION){
                $obj = json_decode( file_get_contents('php://input') );   
                $objArr = (array)$obj;
                if (empty($objArr)){                        
                    $this->response(422,"error","Nothing to add. Check json");                        
                }else if(isset($obj->usuario) && isset($obj->contrasena) ){
                    $db = new UsuarioDB();
                    $db->update($_GET['id'], $obj->usuario, $obj->contrasena);
                    $this->response(200,"success","Record updated");                             
                }else{
                    $this->response(422,"error","The property is not defined");                        
                }     
                exit;
           }
        }
        $this->response(400);
    }
    
    function deleteUsuario(){
        if( isset($_GET['action']) && isset($_GET['id']) ){
            if($_GET['action']==self::API_ACTION){                   
                $db = new UsuarioDB();
                $db->delete($_GET['id']);
                $this->response(204);                   
                exit;
            }
        }
        $this->response(400);
    }
    
    function processPost()
    {
        if($_GET['action']==self::API_ACTION)
        {
            $obj = json_decode( file_get_contents('php://input') );
            $objArr = (array)$obj;
            if (empty($objArr)) {
                $this->response(422,"error","Nothing to add. Check json");
            } else if(isset($obj->auth)) {
                $people = new UsuarioDB();
                $response = $people->authenticate( $obj->auth->usuario,  $obj->auth->contrasena);
                if(count($response)  == 1){
                    $this->response(200,"success","authenticated");
                } else {
                    $this->response(200,"error","not authenticated");
                }
            }else if(isset($obj->usuario) && isset($obj->contrasena)) {
                $people = new UsuarioDB();
                $people->insert( $obj->usuario,  $obj->contrasena);
                $this->response(200,"success","new record added");
            } else {
                $this->response(422,"error","The property is not defined");
            }            
        }
        else
        {
            $this->response(400);
        }
    }
}//end class