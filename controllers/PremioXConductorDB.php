<?php
/**
 * Description of PremioXCuentaDB
 *
 * @author meza
 */
class PremioXConductorDB extends EntityDB {
   protected $mysqli;
   const TABLE = 'premiosxcuenta';
    
    public function getById($id=''){
        $stmt = $this->mysqli->prepare("SELECT * FROM " 
                . self::TABLE . " WHERE id='$id';");
        $stmt->execute();
        $result = $stmt->get_result();
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $stmt->close();
        return $entity;
    }
    
    public function getList(){
        $query = "SELECT x.id, x.idpremio, p.descripcion AS premio, "
                . "p.codigo AS codigopremio, x.idcuenta, c.nombre AS cuenta, "
                . "x.fecpremio, x.fecven, x.idestado, pe.estado, x.idmovil, "
                . "m.nromovil, m.descripcion AS movil, x.idconductor, "
                . "CONCAT (o.nombres, ', ', o.apellidos) AS conductor, x.idbitacora, "
                . "pec.estado AS estadoprexcond, x.idestadoprexcond, x.feccanjeo, "
                . "x.feccanjeocond "
                . "FROM premiosxcuenta x "
                . "LEFT JOIN premios p ON x.idpremio = p.id "
                . "LEFT JOIN cuentas c ON x.idcuenta = c.id "
                . "LEFT JOIN premiosestados pe ON x.idestado = pe.id "
                . "LEFT JOIN premiosestados pec ON x.idestadoprexcond = pec.id "
                . "LEFT JOIN moviles m ON x.idmovil = m.id "
                . "LEFT JOIN conductores o ON x.idconductor = o.id ";
        //var_dump($query);
        $result = $this->mysqli->query($query);
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $result->close();
        return $entity;
    }
    
    public function getByConductorXMovil($id = -1) {
        $query = "SELECT x.id, x.idpremio, p.descripcion AS premio, 
                p.codigo AS codigopremio, p.idsponsor, p.direccion, s.razonsocial AS sponsor,
                x.idcuenta, c.nombre AS cuenta, x.fecpremio, x.fecven, x.idestado, 
                pe.estado, x.idmovil, m.nromovil, m.descripcion AS movil, 
                x.idconductor, CONCAT (o.nombres, ', ', o.apellidos) AS conductor, 
                x.idbitacora, pec.estado AS estadoprexcond, x.idestadoprexcond, 
                x.feccanjeo, x.feccanjeocond 
            FROM premiosxcuenta x
            LEFT JOIN conductoresxmoviles cx ON cx.idmovil = x.idmovil AND cx.idconductor = x.idconductor
            LEFT JOIN premios p ON x.idpremio = p.id 
            LEFT JOIN sponsors s ON s.id = p.idsponsor
            LEFT JOIN cuentas c ON x.idcuenta = c.id 
            LEFT JOIN premiosestados pe ON x.idestado = pe.id 
            LEFT JOIN premiosestados pec ON x.idestadoprexcond = pec.id 
            LEFT JOIN moviles m ON x.idmovil = m.id 
            LEFT JOIN conductores o ON x.idconductor = o.id 
            WHERE cx.id = $id AND p.id IS NOT NULL AND x.fecven >= NOW()";
//        var_dump($query);
//        return true;
        $result = $this->mysqli->query($query);
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $result->close();
        return $entity;
    }

    public function getByIdPremioXConductor ($id) {
        $query = "SELECT x.id, x.idpremio, p.descripcion AS premio,
                p.codigo AS codigopremio, x.idcuenta, p.idsponsor, s.sponsor,
                c.nombre AS cuenta, x.fecpremio, x.fecven, x.idestado, pe.estado, 
                x.idmovil, m.nromovil, m.descripcion AS movil, x.idconductor, 
                CONCAT (o.nombres, ', ', o.apellidos) AS conductor, x.idbitacora, 
                pec.estado AS estadoprexcond, x.idestadoprexcond, x.feccanjeo, 
                x.feccanjeocond 
            FROM premiosxcuenta x 
            LEFT JOIN premios p ON x.idpremio = p.id 
            LEFT JOIN sponsors s ON s.id = p.idsponsor
            LEFT JOIN cuentas c ON x.idcuenta = c.id 
            LEFT JOIN premiosestados pe ON x.idestado = pe.id 
            LEFT JOIN premiosestados pec ON x.idestadoprexcond = pec.id 
            LEFT JOIN moviles m ON x.idmovil = m.id 
            LEFT JOIN conductores o ON x.idconductor = o.id 
            WHERE x.id= '$id'";
        $result = $this->mysqli->query($query);
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $result->close();
        return $entity;
    }

    public function getByIDResumenXEstado($idestado) {
        $query = "SELECT x.id, x.idpremio, IFNULL(p.descripcion, 'Premio Eliminado') AS premio, IFNULL(p.codigo, '') AS codigopremio, x.idcuenta, "
                . "c.nombre AS cuenta, x.fecpremio, x.fecven, x.idestado, pe.estado, "
                . "x.idmovil, IFNULL(m.nromovil, ''), IFNULL(m.descripcion, 'Móvil eliminado') AS movil, x.idconductor, "
                . "IFNULL(CONCAT (o.nombres, ', ', o.apellidos), 'SIN CONDUCTOR ASIGNADO') AS conductor, x.idbitacora, "
                . "pec.estado AS estadoprexcond, x.idestadoprexcond, x.feccanjeo, x.feccanjeocond "
                . "FROM premiosxcuenta x "
                . "LEFT JOIN premios p ON x.idpremio = p.id "
                . "LEFT JOIN cuentas c ON x.idcuenta = c.id "
                . "LEFT JOIN premiosestados pe ON x.idestado = pe.id "
                . "LEFT JOIN premiosestados pec ON x.idestadoprexcond = pec.id "
                . "LEFT JOIN moviles m ON x.idmovil = m.id "
                . "LEFT JOIN conductores o ON x.idconductor = o.id "
                . "WHERE x.idestadoprexcond= '$idestado'";
        //var_dump($query);
        $result = $this->mysqli->query($query);
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $result->close();
        return $entity;
    }
    
    public function getPremiosXFecha($sponsor='', $fecmin='', $fecmax='') {
        $query = "SELECT x.idpremio, s.razonsocial, COUNT(x.idpremio) AS cantidad, p.codigo AS codigopremio, 
                SUM((CASE feccanjeo WHEN '' THEN 0 ELSE 1 END)) AS canjeado,
                SUM((CASE feccanjeocond WHEN '' THEN 0 ELSE 1 END)) AS canjeadocond,
                p.descripcion AS premio, p.imagen 
            FROM premiosxcuenta x 
            LEFT JOIN premios p ON p.id = x.idpremio 
            LEFT JOIN sponsors s ON s.id = p.idsponsor 
            WHERE p.idsponsor = $sponsor 
                AND x.fecpremio BETWEEN '$fecmin' AND '$fecmax' 
            GROUP BY x.idpremio";
        //var_dump($query);
        $result = $this->mysqli->query($query);
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $result->close();
        return $entity;
    }
    
    public function getPremioXFecha($idpremio='', $fecmin='', $fecmax='') {
        $query = "SELECT x.fecpremio, CONCAT(c.apellidos, ', ', c.nombres) AS conductor,
                m.nromovil, (CASE feccanjeo WHEN '' THEN 0 ELSE 1 END) AS canjeado,
                 (CASE feccanjeocond WHEN '' THEN 0 ELSE 1 END) AS canjeadocond,
                 u.nombre, u.email, feccanjeo, feccanjeocond
            FROM premiosxcuenta x 
            LEFT JOIN cuentas u ON u.id = x.idcuenta
            LEFT JOIN premios p ON p.id = x.idpremio 
            LEFT JOIN conductores c ON c.id = x.idconductor 
            LEFT JOIN moviles m ON m.id = x.idmovil 
            WHERE x.idpremio = $idpremio
                AND x.fecpremio BETWEEN '$fecmin' AND '$fecmax' 
            ORDER BY x.fecpremio";
        //var_dump($query);
        $result = $this->mysqli->query($query);
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $result->close();
        return $entity;
    }

    public function insert($id = '', 
            $idpremio = -1, $idcuenta = '', $fecpremio = '', $fecven = '', 
            $idestado = -1, $idmovil = '', $idconductor = '', $idbitacora = '', 
            $feccanjeo = ''){
        $query = "INSERT INTO premiosxcuenta 
                (id, 
                idpremio, idcuenta, fecpremio, fecven, 
                idestado, idmovil, idconductor, idbitacora, 
                idestadoprexcond, feccanjeo) 
            VALUES 
                ('$id', 
                $idpremio, '$idcuenta', '$fecpremio', '$fecven', 
                $idestado, '$idmovil', '$idconductor', '$idbitacora',
                1, '$feccanjeo')";
//        var_dump($query);
//        return $id;
        $stmt = $this->mysqli->prepare($query);
        $r = $stmt->execute();
        
        $stmt->close();
        return $id;
    }
    /*
            $id, 
            $obj->idpremio, $obj->idcuenta, $obj->fecpremio, 
            $obj->fecven, $obj->idestado, $obj->idmovil, 
            $obj->idconductor, $obj->idbitacora, $obj->idestadoprexcond, 
            $obj->feccanjeo, $obj->feccanjeocond
    */

    public function update($id = '', 
            $idpremio = -1, $idcuenta = '', $fecpremio = '', 
            $fecven = '',  $idestado = -1, $idmovil = '', 
            $idconductor = '', $idbitacora = '', $idestadoprexcond = -1, 
            $feccanjeo = '', $feccanjeocond = '') {
        if($this->checkStringID(self::TABLE, $id)){
            $feccanjeo = ($idestado === 2) ? ('NOW()') : ($feccanjeo);
            $query="UPDATE premiosxcuenta SET 
                    idpremio = $idpremio, idcuenta = '$idcuenta', fecpremio = '$fecpremio', fecven = '$fecven', 
                    idestado = $idestado, idmovil = '$idmovil', idconductor = '$idconductor', idbitacora = '$idbitacora', 
                    feccanjeo = '$feccanjeo', feccanjeocond = '$feccanjeocond' 
                WHERE id = '$id';";
//            var_dump($query);
//            return $id;
            $stmt = $this->mysqli->prepare($query);
            $r = $stmt->execute(); 
            $stmt->close();
            return $id;
        } else {
            $feccanjeo = ($idestado === '2') ? ('NOW()') : ($feccanjeo);
            $query = "INSERT INTO premiosxcuenta 
                (id, 
                idpremio, idcuenta, fecpremio, fecven, 
                idestado, idmovil, idconductor, idbitacora, 
                idestadoprexcond, feccanjeo) 
            VALUES 
                ('$id', 
                $idpremio, '$idcuenta', '$fecpremio', '$fecven', 
                $idestado, '$idmovil', '$idconductor', '$idbitacora',
                1, $feccanjeo)";
            $stmt = $this->mysqli->prepare($query);
            $r = $stmt->execute();
            $stmt->close();
            return $id;
        }
        return false;
    }
        
    public function canjeoCond($id = '') {
//        var_dump($id);
        if($this->checkStringID(self::TABLE, $id)){
            $query="UPDATE " . self::TABLE . " SET
                    idestadoprexcond = 4, feccanjeocond = NOW() 
                WHERE id = '$id';";
//             var_dump($query);
//             return true;
            $stmt = $this->mysqli->prepare($query);
            $r = $stmt->execute(); 
            $stmt->close();
            return $r;
        }
        return false;
    }
    
    public function canjeoCuent($id = '', $feccanjeo = '') {
        if ($this->checkStringID(self::TABLE, $id)) {
            $query = "UPDATE " . self::TABLE . " SET "
                    . "idestado = 4, feccanjeo = '$feccanjeo' "
                    . "WHERE id = '$id';";
            $stmt = $this->mysqli->prepare($query);
            $r = $stmt->execute();
            $stmt->close();
            return $r;
        }
        return false;
    }
    
    public function delete($id=0) {
        $stmt = $this->mysqli->prepare("DELETE FROM ". self::TABLE ." WHERE id = ?;");
        $stmt->bind_param('i', $id);
        $r = $stmt->execute(); 
        $stmt->close();
        return $r;
    }
    
    public function checkID($id){
        $stmt = $this->mysqli->prepare("SELECT * FROM " . self::TABLE 
                . " WHERE ID=?");
        $stmt->bind_param("i", $id);
        if($stmt->execute()){
            $stmt->store_result();    
            if ($stmt->num_rows == 1){                
                return true;
            }
        }        
        return false;
    }
}
