<?php
/**
 * Description of ConductorxMovilDB
 *
 * @author meza
 */
class ConductorxMovilDB extends EntityDB {
   protected $mysqli;
   const TABLE = 'conductoresxmoviles';
    
    public function getById($id=0){
        $stmt = $this->mysqli->prepare("SELECT cm.id, cm.idconductor, CONCAT (c.apellidos, ', ', c.nombres) AS conductor, cm.idmovil, m.nromovil, cm.activo 
            FROM conductores c
            LEFT JOIN conductoresxmoviles cm  ON cm.idconductor = c.id 
            LEFT JOIN moviles m ON m.id = cm.idmovil 
            WHERE cm.id=?;");
        $stmt->bind_param('i', $id);
        $stmt->execute();
        $result = $stmt->get_result();
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $stmt->close();
        return $entity;
    }
    
    public function getList(){
        $query = "SELECT cm.id, cm.idconductor, CONCAT (c.apellidos, ', ', c.nombres) AS conductor, cm.idmovil, m.nromovil, cm.activo 
            FROM conductores c
            LEFT JOIN conductoresxmoviles cm  ON cm.idconductor = c.id 
            LEFT JOIN moviles m ON m.id = cm.idmovil 
            WHERE cm.activo = 1 AND m.nromovil IS NOT NULL
            ORDER BY c.apellidos, c.nombres;";
        $result = $this->mysqli->query($query);
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $result->close();
        return $entity;
    }
    
  /*  public function insert($id='', $idconductor=-1, $idmovil=-1){
        $stmt = $this->mysqli->prepare(
                "INSERT INTO " . self::TABLE . " (idconductor, idmovil)"
                . "VALUES (?, ?);");
        $stmt->bind_param('ii', $id, $idconductor, $idmovil);
        $r = $stmt->execute();
        
        $stmt->close();
        return $r;
    }
    public function delete($id=0) {
        $stmt = $this->mysqli->prepare("DELETE FROM ". self::TABLE ." WHERE id = ?;");
        $stmt->bind_param('i', $id);
        $r = $stmt->execute(); 
        $stmt->close();
        return $r;
    }
    public function update($id='', $idconductor=-1, $idmovil=-1) {
        if($this->checkID($id)){
            $stmt = $this->mysqli->prepare(
                    "UPDATE " . self::TABLE . " SET idconductor=?, idmovil=? "
                    . "WHERE id = ?;");
            $stmt->bind_param('iii', $idconductor, $idmovil, $id);
            $r = $stmt->execute(); 
            $stmt->close();
            return $r;
        }
        return false;
    }
    
     public function checkID($id){
        $stmt = $this->mysqli->prepare("SELECT * FROM " . self::TABLE 
                . " WHERE ID=?");
        $stmt->bind_param("i", $id);
        if($stmt->execute()){
            $stmt->store_result();    
            if ($stmt->num_rows == 1){                
                return true;
            }
        }        
        return false;
    }*/
}