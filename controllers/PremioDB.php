<?php
/**
 * Description of PremioDB
 *
 * @author meza
 */
class PremioDB extends EntityDB {
   protected $mysqli;
   const TABLE = 'premios';
    
    public function getById($id=0){
        $query = "SELECT p.id, p.idsponsor, s.razonsocial AS sponsor, 
                p.direccion, p.descripcion, p.fecinicio, p.fecfin, 
                p.idpaquete, a.paquete,p.idclasificacion, c.clasificacion, 
                p.imagen, p.fecven, p.codigo, p.fecmodificacion 
            FROM premios p 
            LEFT JOIN sponsors s ON p.idsponsor = s.id 
            LEFT JOIN paquetes a ON p.idpaquete = a.id 
            LEFT JOIN clasificaciones c ON p.idclasificacion = c.id
            WHERE p.id=$id;";
//        var_dump($query);
        $stmt = $this->mysqli->prepare($query);
        $stmt->execute();
        $result = $stmt->get_result();
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $stmt->close();
        return $entity;
    }
    
    public function getListAll(){
        $result = $this->mysqli->query(
                "SELECT p.id, p.idsponsor, s.razonsocial AS sponsor, p.descripcion, p.fecinicio, p.fecfin, "
                . "p.idpaquete, a.paquete,p.idclasificacion, c.clasificacion, "
                . "p.imagen, p.fecven, p.codigo, p.fecmodificacion "
                . "FROM premios p "
                . "LEFT JOIN sponsors s ON p.idsponsor = s.id "
                . "LEFT JOIN paquetes a ON p.idpaquete = a.id "
                . "LEFT JOIN clasificaciones c ON p.idclasificacion = c.id");
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $result->close();
        return $entity;
    }
    
     public function getByNews($fecmodif){
//        $informat = '%Y%m%d%H%M%S'; 
//        $outformat =  '%Y-%m-%d %H:%M:%S'; 
//        $ftime = $this->strptime($fecmodif, $informat);
//        $unxTimestamp = mktime($ftime['tm_hour'], $ftime['tm_min'], $ftime['tm_sec'], 1, $ftime['tm_yday'] + 1, $ftime['tm_year'] + 1900); 
//        $fecmodif = strftime($outformat , $unxTimestamp); 
        // var_dump($fecmodif1);
        $query="SELECT p.id, p.idsponsor, s.razonsocial AS sponsor, p.descripcion, p.fecinicio, p.fecfin, 
                p.idpaquete, a.paquete, p.idclasificacion, c.clasificacion, p.direccion, 
                p.imagen, p.fecven, p.codigo, IFNULL(p.fecmodificacion, '') AS fecmodificacion 
                FROM premios p 
                LEFT JOIN sponsors s ON p.idsponsor = s.id 
                LEFT JOIN paquetes a ON p.idpaquete = a.id 
                LEFT JOIN clasificaciones c ON p.idclasificacion = c.id 
                WHERE (SELECT COUNT(id) As cant FROM `premios` WHERE DATE_FORMAT(fecmodificacion, '%Y%m%d%H%i%s') > '$fecmodif') > 0 
                AND p.fecfin > NOW()";
//        var_dump($query);
        $result = $this->mysqli->query($query);
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $result->close();
        return $entity;
        //return true;
    }
    
    function strptime($date, $format) { 
        $masks = array( 
          '%d' => '(?P<d>[0-9]{2})', 
          '%m' => '(?P<m>[0-9]{2})', 
          '%Y' => '(?P<Y>[0-9]{4})', 
          '%H' => '(?P<H>[0-9]{2})', 
          '%M' => '(?P<M>[0-9]{2})', 
          '%S' => '(?P<S>[0-9]{2})', 
         // usw.. 
        ); 

        $rexep = "#".strtr(preg_quote($format), $masks)."#"; 
        if(!preg_match($rexep, $date, $out)) 
          return false; 

        $ret = array( 
          "tm_sec"  => (int) $out['S'], 
          "tm_min"  => (int) $out['M'], 
          "tm_hour" => (int) $out['H'], 
          "tm_mday" => (int) $out['d'], 
          "tm_mon"  => $out['m']?$out['m']-1:0, 
          "tm_year" => $out['Y'] > 1900 ? $out['Y'] - 1900 : 0, 
        ); 
        return $ret; 
    }
    
    public function getList(){
        $result = $this->mysqli->query(
                "SELECT id, idsponsor, descripcion, fecinicio, fecfin, idpaquete, "
                . "idclasificacion, imagen, fecven, codigo, null AS imagenb "
                . "FROM premios "
                . "WHERE NOW() >= fecinicio and NOW() <= fecfin");
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $result->close();
        return $entity;
    }
    
    public function insert($idsponsor=-1, $descripcion='', $direccion='',
            $fecinicio='', $fecfin='', $idpaquete=-1, 
            $idclasificacion=-1, $imagen='', $fecven='', 
            $codigo=''){
        $query= "INSERT INTO " . self::TABLE . " 
                (idsponsor, descripcion, direccion, 
                fecinicio, fecfin, idpaquete, 
                idclasificacion, imagen, fecven, 
                codigo, fecmodificacion) 
            VALUES 
                ($idsponsor, '$descripcion', '$direccion', 
                '$fecinicio', '$fecfin', $idpaquete, 
                $idclasificacion, '$imagen', '$fecven', 
                '$codigo', NOW());";
//        var_dump($query);
        $stmt = $this->mysqli->prepare($query);
        $r = $stmt->execute();
        
        $stmt->close();
        return $r;
    }
    
    public function update($id=-1, 
            $idsponsor=-1, $descripcion='', $direccion='', 
            $fecinicio='', $fecfin='', $idpaquete=-1, 
            $idclasificacion=-1, $imagen='', $fecven='', 
            $codigo='') {
        if($this->checkID($id)){
            $query = "UPDATE " . self::TABLE . 
                    " SET idsponsor=$idsponsor, descripcion='$descripcion', direccion='$direccion', 
                    fecinicio='$fecinicio', fecfin='$fecfin', idpaquete=$idpaquete, 
                    idclasificacion=$idclasificacion, imagen='$imagen', fecven='$fecven', 
                    codigo='$codigo', fecmodificacion= NOW() 
                    WHERE id = $id;";
            
            $stmt = $this->mysqli->prepare($query);
            //var_dump($query);
            $r = $stmt->execute(); 
            $stmt->close();
            return $r;
        }
        return false;
    }
    
    public function delete($id=0) {
        $stmt = $this->mysqli->prepare("DELETE FROM ". self::TABLE ." WHERE id = ?;");
        $stmt->bind_param('i', $id);
        $r = $stmt->execute(); 
        $stmt->close();
        return $r;
    }
    
     public function checkID($id){
        $stmt = $this->mysqli->prepare("SELECT * FROM " . self::TABLE 
                . " WHERE ID=?");
        $stmt->bind_param("i", $id);
        if($stmt->execute()){
            $stmt->store_result();    
            if ($stmt->num_rows == 1){                
                return true;
            }
        }        
        return false;
    }
}
