<?php
/**
 * Description of UsuarioDb
 *
 * @author WebDev
 */
class UsuarioDB extends EntityDB {
   const TABLE = 'usuarios';
   protected $mysqli;
    
    public function getUsuario($id=0){
        $stmt = $this->mysqli->prepare("SELECT * FROM usuarios WHERE id=?;");
        $stmt->bind_param('i', $id);
        $stmt->execute();
        $result = $stmt->get_result();
        $usuario = $result->fetch_all(MYSQLI_ASSOC);
        $stmt->close();
        return $usuario;
    }
    
    public function getUsuarios(){
        $result = $this->mysqli->query('SELECT * FROM usuarios');
        $usuarios = $result->fetch_all(MYSQLI_ASSOC);
        $result->close();
        return $usuarios;
    }
    
    public function insert($usuario='', $contrasena=''){
        $stmt = $this->mysqli->prepare(
                "INSERT INTO usuarios (usuario, contrasena) VALUES (?, ?);");
        $stmt->bind_param('ss', $usuario, $contrasena);
        $r = $stmt->execute();
        $stmt->close();
        return $r;
    }
    
    public function delete($id=0) {
        $stmt = $this->mysqli->prepare("DELETE FROM ". self::TABLE ." WHERE id = ?;");
        $stmt->bind_param('i', $id);
        $r = $stmt->execute(); 
        $stmt->close();
        return $r;
    }
    
    public function update($id, $n_usuario, $n_contrasena) {
        if($this->checkID($id)){
            $stmt = $this->mysqli->prepare(
                    "UPDATE usuarios SET usuario=?, contrasena=? WHERE id = ?;");
            $stmt->bind_param('ssi', $n_usuario, $n_contrasena, $id);
            $r = $stmt->execute(); 
            $stmt->close();
            return $r;    
        }
        return false;
    }
    
    public function authenticate($usuario='', $contrasena=''){
        $stmt = $this->mysqli->prepare(
                "SELECT * FROM usuarios WHERE usuario=? AND contrasena=?");
        $stmt->bind_param('ss', $usuario, $contrasena);
        $stmt->execute();        
        $result = $stmt->get_result();
        $usuario = $result->fetch_all(MYSQLI_ASSOC);
        $stmt->close();
        
        return $usuario;
    }
    
     public function checkID($id){
        $stmt = $this->mysqli->prepare("SELECT * FROM usuarios WHERE ID=?");
        $stmt->bind_param("i", $id);
        if($stmt->execute()){
            $stmt->store_result();    
            if ($stmt->num_rows == 1){                
                return true;
            }
        }        
        return false;
    }
}
