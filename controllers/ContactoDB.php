<?php
/**
 * Description of ContactoDB
 *
 * @author meza
 */
class ContactoDB extends EntityDB {
   protected $mysqli;
   const TABLE = 'contactos';
    
    public function getById($id=0){
        $stmt = $this->mysqli->prepare("SELECT * FROM " 
                . self::TABLE . " WHERE id=?;");
        $stmt->bind_param('i', $id);
        $stmt->execute();
        $result = $stmt->get_result();
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $stmt->close();
        return $entity;
    }
  
    public function getListByIdSponsors($id=1){
        $query = "SELECT c.id, c.apellidos, c.nombres, c.email, c.telefono, c.cargo "
                . "FROM contactos c "
                . "LEFT JOIN contactosxsponsors x ON c.id = x.idcontacto "
                . "WHERE x.idsponsor=" . $id;
        $result = $this->mysqli->query($query);
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $result->close();
        return $entity;
    }
    
    public function getList(){
        $query = "SELECT c.id, c.apellidos, c.nombres, c.email, c.telefono, c.cargo 
            FROM contactos c;";
        $result = $this->mysqli->query($query);
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $result->close();
        return $entity;
    }
    
    public function insert($apellidos='', $nombres='', 
        $email='', $telefono='', $cargo='', $idsponsor=-1){
        $stmt = $this->mysqli->prepare(
                "INSERT INTO " . self::TABLE . " (apellidos, nombres, "
                . "email, telefono, cargo) "
                . "VALUES (?, ?, ?, ?, ?);");
        $stmt->bind_param('sssss', $apellidos, $nombres, $email, $telefono, $cargo);
        $r = $stmt->execute();
        $stmt->close();
        
        $idcontacto = $this->mysqli->insert_id;
        return $this->insertCxS($idcontacto, $idsponsor);
        //return $r;
    }
    
    public function insertCxS($idcontacto=-1, $idsponsor=-1){
        $stmt = $this->mysqli->prepare(
                "INSERT INTO contactosxsponsors (idcontacto, idsponsor) "
                . "VALUES (?, ?);");
        $stmt->bind_param('ii', $idcontacto, $idsponsor);
        $r = $stmt->execute();
        
        $stmt->close();
        return $r;
    }
    
    public function delete($id=0) {
        $stmt = $this->mysqli->prepare("DELETE FROM ". self::TABLE ." WHERE id = ?;");
        $stmt->bind_param('i', $id);
        $r = $stmt->execute(); 
        $stmt->close();
        return $r;
    }
    public function update($id=-1, $apellidos='', $nombres='', 
            $email='', $telefono='', $cargo='') {
        if($this->checkID($id)){
            $stmt = $this->mysqli->prepare(
                    "UPDATE " . self::TABLE . " SET apellidos=?, nombres=?, "
                    . "email=?, telefono=?, cargo=? "
                    . "WHERE id = ?;");
            $stmt->bind_param('sssssi', $apellidos, $nombres, $email, $telefono, $cargo, $id);
            $r = $stmt->execute(); 
            $stmt->close();
            return $r;
        }
        return false;
    }
    
     public function checkID($id){
        $stmt = $this->mysqli->prepare("SELECT * FROM " . self::TABLE 
                . " WHERE ID=?");
        $stmt->bind_param("i", $id);
        if($stmt->execute()){
            $stmt->store_result();    
            if ($stmt->num_rows == 1){                
                return true;
            }
        }        
        return false;
    }
}
