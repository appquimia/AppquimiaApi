<?php
/**
 * Description of PaquetesDB
 *
 * @author meza
 */
class PaqueteDB extends EntityDB {
   protected $mysqli;
   const TABLE = 'paquetes';
    
    public function getById($id=0){
        $stmt = $this->mysqli->prepare("SELECT * FROM " 
                . self::TABLE . " WHERE id=?;");
        $stmt->bind_param('i', $id);
        $stmt->execute();
        $result = $stmt->get_result();
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $stmt->close();
        return $entity;
    }
	
    public function getList(){
        $result = $this->mysqli->query(
                "SELECT * FROM ". self::TABLE);
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $result->close();
        return $entity;
    }
	
    public function insert($paquete=""){
        $stmt = $this->mysqli->prepare(
                "INSERT INTO " . self::TABLE . " (paquete) "
                . "VALUES (?);");
        $stmt->bind_param('s', $paquete);
        $r = $stmt->execute();

        $stmt->close();
        return $r;
    }
    public function delete($id=0) {
        $stmt = $this->mysqli->prepare("DELETE FROM ". self::TABLE ." WHERE id = ?;");
        $stmt->bind_param('i', $id);
        $r = $stmt->execute(); 
        $stmt->close();
        return $r;
    }	
    public function update($id=-1, $paquete="") {
        if($this->checkID($id)){
            $stmt = $this->mysqli->prepare(
                    "UPDATE " . self::TABLE . " SET paquete=? "
                    . "WHERE id = ?;");
            $stmt->bind_param('si', $paquete,$id);
            $r = $stmt->execute(); 
            $stmt->close();
            return $r;
        }
        return false;
    }
	public function checkID($id){
        $stmt = $this->mysqli->prepare("SELECT * FROM " . self::TABLE 
                . " WHERE ID=?");
        $stmt->bind_param("i", $id);
        if($stmt->execute()){
            $stmt->store_result();    
            if ($stmt->num_rows == 1){                
                return true;
            }
        }        
        return false;
    }
}
