<?php
/**
 * Description of TarifasFijasDB
 *
 * @author meza
 */
class TarifaFijaDB extends EntityDB {
   protected $mysqli;
   const TABLE = 'tarifasfijas';
    
    public function getById($id=0){
        $stmt = $this->mysqli->prepare("SELECT * FROM " 
                . self::TABLE . " WHERE id=?;");
        $stmt->bind_param('i', $id);
        $stmt->execute();
        $result = $stmt->get_result();
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $stmt->close();
        return $entity;
    }
    
    public function getList(){
        $result = $this->mysqli->query(
                "SELECT t.id, t.fecha, t.destino, t.precio "
                . "FROM tarifasfijas t ");
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $result->close();
        return $entity;
    }
    
    public function insert($fecha='', $destino='', 
        $precio=-1){
        $stmt = $this->mysqli->prepare(
                "INSERT INTO " . self::TABLE . " (fecha, destino, "
                . "precio) "
                . "VALUES (?, ?, ?);");
        $stmt->bind_param('ssd', $fecha, $destino, $precio);
        $r = $stmt->execute();
        
        $stmt->close();
        return $r;
    }
    public function delete($id=0) {
        $stmt = $this->mysqli->prepare("DELETE FROM ". self::TABLE ." WHERE id = ?;");
        $stmt->bind_param('i', $id);
        $r = $stmt->execute(); 
        $stmt->close();
        return $r;
    }
    public function update($id=-1, $fecha='', $destino='', 
            $precio=-1) {
        if($this->checkID($id)){
            $stmt = $this->mysqli->prepare(
                    "UPDATE " . self::TABLE . " SET fecha=?, destino=?, "
                    . "precio=? "
                    . "WHERE id = ?;");
            $stmt->bind_param('ssdi', $fecha, $destino, $precio, $id);
            $r = $stmt->execute(); 
            $stmt->close();
            return $r;
        }
        return false;
    }
    
     public function checkID($id){
        $stmt = $this->mysqli->prepare("SELECT * FROM " . self::TABLE 
                . " WHERE ID=?");
        $stmt->bind_param("i", $id);
        if($stmt->execute()){
            $stmt->store_result();    
            if ($stmt->num_rows == 1){                
                return true;
            }
        }        
        return false;
    }
}
