<?php
/**
 * Description of BitacoraAPI
 *
 * @author meza
 */
class BitacoraAPI extends EntityAPI {
    const API_ACTION = 'bitacora';
    const PREFIX_CUENTAS= 'c';
    
   function processGet() {
        if (isset($_GET['id'])) {
            $id = $_GET['id'];
            $isCuentas = strpos($id, self::PREFIX_CUENTAS);

            if ($isCuentas !== false) {
                $response = $this->db->getBitacoraByIdCuenta(substr($id, 1));
                echo json_encode($response, JSON_PRETTY_PRINT);
            } else {
                $response = $this->db->getById($id);
                echo json_encode($response, JSON_PRETTY_PRINT);
            }
        } else {
            $this->response(400);
        }
    }

    function processPost() {
        $obj = json_decode( file_get_contents('php://input') );
        $objArr = (array)$obj;
        if (empty($objArr)) {
            $this->response(422,"error","Nothing to add. Check json");
        } else if(isset($obj->idcuenta) AND isset($obj->idmovil) 
                AND isset($obj->fecha) AND isset($obj->costo)) {
            $r = $this->db->insert($obj->idcuenta, $obj->idmovil, 
                    $obj->fecha,$obj->costo);
            if($r)
                $this->response(200,"success","new record added");
        } else {
            $this->response(422,"error","The property is not defined");
        }
    }
    
    function processPut() {
        if( isset($_GET['action']) && isset($_GET['id']) ){
            $obj = json_decode( file_get_contents('php://input') );   
            $objArr = (array)$obj;
            if (empty($objArr)){                        
                $this->response(422,"error","Nothing to add. Check json");                        
            }else if(isset($obj->idcuenta) AND isset($obj->idmovil) 
                AND isset($obj->fecha) AND isset($obj->costo)){
                if($this->db->update($_GET['id'], $obj->idcuenta, $obj->idmovil, 
                    $obj->fecha,$obj->costo))
                    $this->response(200,"success","Record updated");                             
                else
                    $this->response(304,"success","Record not updated");                             
            }else{
                $this->response(422,"error","The property is not defined");                        
            }     
            exit;
        }
        $this->response(400);
    }
    
    function processDelete() {
        if (isset($_GET['action']) && isset($_GET['id'])) {
            if ($_GET['action'] == self::API_ACTION) {
                $this->db->delete($_GET['id']);
                $this->response(204);
                exit;
            }
        }
        $this->response(400);
    }
}
