<?php
/**
 * Description of PublicidadDB
 *
 * @author meza
 */
class PublicidadDB extends EntityDB {
    protected $mysqli;
    const TABLE = 'publicidades';
    
    public function getById($id=0){
        $stmt = $this->mysqli->prepare("SELECT * FROM " 
                . self::TABLE . " WHERE id=?;");
        $stmt->bind_param('i', $id);
        $stmt->execute();
        $result = $stmt->get_result();
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $stmt->close();
        return $entity;
    }
    
    public function getList(){
        $result = $this->mysqli->query(
                "SELECT p.id, p.publicidad, p.url, p.fecinicio, p.fecfin, "
                . "p.idclasificacion, fecmodificacion "
                . "FROM publicidades p ");
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $result->close();
        return $entity;
    }
    
    public function getByNews($fecmodif){
        $result = $this->mysqli->query(
                "SELECT p.id, p.publicidad, p.url, p.fecinicio, p.fecfin, "
                . "p.idclasificacion, IFNULL(p.fecmodificacion, '') AS fecmodificacion "
                . "FROM publicidades p "
                . "WHERE (SELECT COUNT(id) As cant FROM `premios` WHERE fecmodificacion > '$fecmodif') > 0");
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $result->close();
        return $entity;
    }
    
    public function insert($publicidad='', $url='', 
        $fecinicio='', $fecfin='', $idclasificacion=-1 ){
        $query = "INSERT INTO " . self::TABLE . " (publicidad, url, "
                . "fecinicio, fecfin, idclasificacion, fecmodificacion) "
                . "VALUES ( "
                . "'$publicidad', '$url', '$fecinicio', "
                . "'$fecfin', $idclasificacion, NOW());";
//        var_dump($query);
        $stmt = $this->mysqli->prepare($query);
        $r = $stmt->execute();
        
        $stmt->close();
        return $r;
    }
    public function delete($id=0) {
        $stmt = $this->mysqli->prepare("DELETE FROM ". self::TABLE ." WHERE id = ?;");
        $stmt->bind_param('i', $id);
        $r = $stmt->execute(); 
        $stmt->close();
        return $r;
    }
    public function update($id=-1, $publicidad='', $url='', 
            $fecinicio='', $fecfin='', $idclasificacion=-1 ) {
        if($this->checkID($id)){
            $stmt = $this->mysqli->prepare(
                    "UPDATE " . self::TABLE . " SET "
                    . "publicidad= '$publicidad', url= '$url', "
                    . "fecinicio= '$fecinicio', fecfin= '$fecfin', "
                    . "idclasificacion= $idclasificacion, "
                    . "fecmodificacion= NOW() "
                    . "WHERE id = '$id';");
            $r = $stmt->execute(); 
            $stmt->close();
            return $r;
        }
        return false;
    }
    
     public function checkID($id){
        $stmt = $this->mysqli->prepare("SELECT * FROM " . self::TABLE 
                . " WHERE ID=?");
        $stmt->bind_param("i", $id);
        if($stmt->execute()){
            $stmt->store_result();    
            if ($stmt->num_rows == 1){                
                return true;
            }
        }        
        return false;
    }
}
