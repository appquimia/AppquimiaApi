<?php
/**
 * Description of PremioXCuentaAPI
 *
 * @author meza
 */
class PremioXConductorAPI extends EntityAPI {
    const API_ACTION = 'premioxconductor';
    const GET_BYID = 'byid';
    const GET_RESUMENXESTADO = 'resumenxestado';
    const GET_BYCONDUCTORXMOVIL = 'byconductorxmovil';
    const PUT_CANJEOCOND = 'canjeocond';
    const PUT_CANJEOCUENT = 'canjeocuent';
    const PATCH_PREMIOSXFECHA = 'premiosxfecha';
    const PATCH_PREMIOXFECHA = 'premioxfecha';

    public function __construct() {
        $this->db = new PremioXConductorDB();
        $this->fields = [];
        array_push($this->fields, 
                'id',
                'idpremio',
                'idcuenta',
                'fecpremio',
                'fecven',
                'idestado',
                'idmovil',
                'idconductor',
                'idbitacora',
                'feccanjeo');
    }

    function processGet() {
        $id = filter_input(INPUT_GET, 'id');
        if ($id) {
            $isById = isset($id) ? $id === self::GET_BYID : false; // strpos($id, self::GET_BYID);
            $isResumen = isset($id) ? $id === self::GET_RESUMENXESTADO : false; // = strpos($id, self::GET_RESUMENXESTADO);
            $isByConductorXMovil = isset($id) ? $id === self::GET_BYCONDUCTORXMOVIL : false; // = strpos($id, self::GET_BYCONDUCTORXMOVIL);            
            if ($isById) {
                $fld1 = filter_input(INPUT_GET, 'fld1');
                $response = $this->db->getByIdPremioXConductor($fld1);
                echo json_encode($response, JSON_PRETTY_PRINT);
            } elseif ($isResumen)  {
                $fld1 = filter_input(INPUT_GET, 'fld1');
                $response = $this->db->getByIDResumenXEstado($fld1);
                echo json_encode($response, JSON_PRETTY_PRINT);
            } elseif ($isByConductorXMovil)  {
                $fld1 = filter_input(INPUT_GET, 'fld1');
                $response = $this->db->getByConductorXMovil($fld1);
                echo json_encode($response, JSON_PRETTY_PRINT);
            }else{
                $response = $this->db->getById($id);
                echo json_encode($response, JSON_PRETTY_PRINT);
            }
        } else {
            $response = $this->db->getList();
            echo json_encode($response, JSON_PRETTY_PRINT);
        }
    }
    
    function processPatch() {
        $id = filter_input(INPUT_GET, 'id');
        $obj = json_decode(file_get_contents('php://input'));
        $objArr = (array) $obj;
        $isPremiosXFecha = substr($id, 0, strlen(self::PATCH_PREMIOSXFECHA)) === self::PATCH_PREMIOSXFECHA;
        $isPremioXFecha = substr($id, 0, strlen(self::PATCH_PREMIOXFECHA)) === self::PATCH_PREMIOXFECHA;
        
        if ($isPremiosXFecha) {
            if (isset($obj->idsponsor) AND isset($obj->fecmin) AND isset($obj->fecmax)) {
                $response = $this->db->getPremiosXFecha($obj->idsponsor, $obj->fecmin, $obj->fecmax);
                echo json_encode($response, JSON_PRETTY_PRINT);
            } else {
                $this->response(400);
            }
        } elseif ($isPremioXFecha) {
            if (isset($obj->idpremio) AND isset($obj->fecmin) AND isset($obj->fecmax)) {
                $response = $this->db->getPremioXFecha($obj->idpremio, $obj->fecmin, $obj->fecmax);
                echo json_encode($response, JSON_PRETTY_PRINT);
            } else {
                $this->response(400);
            }
        } else {
            $this->response(400);
        }
    }

    function processPost() {
        $obj = json_decode(file_get_contents('php://input'));
        $objArr = (array) $obj;
        if (empty($objArr)) {
            $this->response(420, "error", "Nothing to add. Check json");
            exit;
        }
        
        if(!$this->checkFields($obj)) {
            $this->response(422,"error","The property is not defined");
            exit;
        }
        $r = $this->db->insert($obj->id, 
            $obj->idpremio, $obj->idcuenta, $obj->fecpremio, 
            $obj->fecven, $obj->idestado, $obj->idmovil, 
            $obj->idconductor, $obj->idbitacora, $obj->feccanjeo);
        if($r) {
            $this->response(200,"id",$obj->id);
        } else {
            $this->response(421,"error","Duplicado");
        }
    }

    function processPut() {
        $id = filter_input(INPUT_GET, 'id');
        if (!$id) {
            $this->response(400);
            exit;
        }
        $isCanjeoCond = isset($id) ? $id === self::PUT_CANJEOCOND : false; // = strpos($id, self::PUT_CANJEOCOND);
        $isCanjeoCuent = isset($id) ? $id === self::PUT_CANJEOCUENT : false; // = strpos($id, self::PUT_CANJEOCUENT);
        
        $obj = json_decode(file_get_contents('php://input'));
        $objArr = (array) $obj;
        if (empty($objArr)) {
            $this->response(421, "error", "Nothing to add. Check json");
            exit;
        }
        if ($isCanjeoCuent AND isset($obj->id) AND isset($obj->feccanjeo)) {
            if ($this->db->canjeoCuent($obj->id, $obj->feccanjeo)) {
                $this->response(200, "success", "Record updated");
            } else {
                $this->response(304, "success", "Record not updated");
            }
        } elseif ($isCanjeoCond AND isset($obj->id)) {
            if ($this->db->canjeoCond($obj->id)) {
                $this->response(200, "success", "Record updated");
            } else{
                $this->response(304, "success", "Record not updated");
            }
        } else {
            if (isset($obj->idpremio)
                    AND isset($obj->idcuenta) AND isset($obj->fecpremio) AND isset($obj->fecven) 
                    AND isset($obj->idestado) AND isset($obj->idmovil) AND isset($obj->idconductor)
                    AND isset($obj->idbitacora) AND isset($obj->idestadoprexcond) AND isset($obj->feccanjeo) 
                    AND isset($obj->feccanjeocond)) {
                if ($this->db->update($id, 
                        $obj->idpremio, $obj->idcuenta, $obj->fecpremio, 
                        $obj->fecven, $obj->idestado, $obj->idmovil, 
                        $obj->idconductor, $obj->idbitacora, $obj->idestadoprexcond, 
                        $obj->feccanjeo, $obj->feccanjeocond)) {
                    $this->response(200, "success", $id);
                } else {
                    $this->response(304, "success", "Record not updated");
                }
            }else {
                $this->response(422, "error", "The property is not defined");
            }
        }
    }

    function processDelete() {
        $id = filter_input(INPUT_GET, 'id');
        if ($id) {
            $this->db->delete($id);
            $this->response(204);
            exit;
        }
        $this->response(400);
    }
}