<?php
/**
 * Description of ReclamoAPI
 *
 * @author meza
 */
class ReclamoAPI extends EntityAPI {
    const API_ACTION = 'reclamo';
    const PREFIX_ESTADOS = 'e';
    const PREFIX_SPONSORS = 's';
    const PREFIX_RESUMEN = 'r';
    
    public function API(){
        header('Content-Type: application/JSON');                
        $method = $_SERVER['REQUEST_METHOD'];
		$this->db = new ReclamoDB();
		
        switch ($method) {
            case 'GET'://consulta
                $this->processGet();
                break;
            case 'POST'://inserta
                $this->processPost();
                break;
            case 'PUT'://actualiza
                $this->processPut();
                break;
            case 'DELETE'://elimina
                $this->processDelete();
                break;
            case 'PATCH'://metodos de procesos
                $this->processPatch();
                break;
            default://metodo NO soportado
                echo 'METODO NO SOPORTADO';
                break;
        }
    }
    
   function processGet() {
        if ($_GET['action'] == self::API_ACTION) {
            if (isset($_GET['id'])) {
                $id = $_GET['id'];
                $isEstado = strpos($id, self::PREFIX_ESTADOS);
                $isIdSponsor = strpos($id, self::PREFIX_SPONSORS);                
                $isResumen = strpos($id, self::PREFIX_RESUMEN);
                
                if ($isEstado !== false && $isEstado === 0) {
                    $response = $this->db->getReclamosByIdEstado(substr($id, 1));
                    echo json_encode($response, JSON_PRETTY_PRINT);
                } elseif ($isIdSponsor !== false && $isIdSponsor === 0) {
                    $response = $this->db->getListReclamosByIdSponsors(substr($id, 1));
                    echo json_encode($response, JSON_PRETTY_PRINT);
                } elseif ($isResumen !== false && $isResumen === 0) {
                    $response = $this->db->getResumen();
                    echo json_encode($response, JSON_PRETTY_PRINT);
                } /*else {
                    $response = $this->db->getById($id);
                    echo json_encode($response, JSON_PRETTY_PRINT);
                }*/
            } else {
                $this->response(400);
            }
        } else {
            $this->response(400);
        }
    }

    /*
     * Sólo se usa desde app móvil
     */
    function processPost() {
        if($_GET['action']==self::API_ACTION)
        {
            $obj = json_decode( file_get_contents('php://input') );
            $objArr = (array)$obj;
            if (empty($objArr)) {
                $this->response(422,"error","Nothing to add. Check json");
            } else if(isset($obj->id) AND isset($obj->idpremio) 
                    AND isset($obj->idcuenta) AND isset($obj->idestado) 
                    AND isset($obj->fecha) AND isset($obj->hora)) {
                $r = $this->db->insert($obj->id, $obj->idpremio, 
                        $obj->idcuenta, $obj->idestado,
                        $obj->fecha, $obj->hora);
                
                if($r)
                    $this->response(200,"id",$obj->id);
                else
                    $this->response(422,"error","Duplicado");
            } else {
                $this->response(422,"error","The property is not defined");
            } 
        }
        else
        {
            $this->response(400);
        }
    }
    
    function processPut() {
        if( isset($_GET['action']) && isset($_GET['id']) ){
            if($_GET['action']==self::API_ACTION){
                $obj = json_decode( file_get_contents('php://input') );   
                $objArr = (array)$obj;
                if (empty($objArr)){                        
                    $this->response(422,"error","Nothing to add. Check json");                        
                }else if(isset($obj->idpremio) AND isset($obj->idcuenta) 
                        AND isset($obj->idestado) AND isset($obj->fecha) 
                        AND isset($obj->hora)){
                    if($this->db->update($_GET['id'], $obj->idpremio, $obj->idcuenta, 
                        $obj->idestado, $obj->fecha, $obj->hora))
                        $this->response(200,"success","Record updated");                             
                    else
                        $this->response(304,"success","Record not updated");                             
                }else{
                    $this->response(422,"error","The property is not defined");                        
                }     
                exit;
           }
        }
        $this->response(400);
    }

    function processPatch() {
        if ($_GET['action'] == self::API_ACTION) {
            $obj = json_decode(file_get_contents('php://input'));
            $objArr = (array) $obj;
            
            if (isset($obj->id)) {
                $response = $this->db->getById($obj->id);
                echo json_encode($response, JSON_PRETTY_PRINT);
            } else {
                $this->response(400);
            }
        }
    }
    
    function processDelete() {
        if (isset($_GET['action']) && isset($_GET['id'])) {
            if ($_GET['action'] == self::API_ACTION) {
                $this->db->delete($_GET['id']);
                $this->response(204);
                exit;
            }
        }
        $this->response(400);
    }   
}