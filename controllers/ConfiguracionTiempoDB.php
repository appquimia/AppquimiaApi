<?php
/**
 * Description of ConfiguracionTiempoDB
 *
 * @author meza
 */
class ConfiguracionTiempoDB extends EntityDB {
   protected $mysqli;
   const TABLE = 'configuracionestiempos';
    
    public function getById($id=0){
        $stmt = $this->mysqli->prepare("SELECT * FROM " 
                . self::TABLE . " WHERE id=$id;");
        $stmt->execute();
        $result = $stmt->get_result();
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $stmt->close();
        return $entity;
    }
    
    public function getList(){
        $query = "SELECT *
            FROM configuracionestiempos;";
//        var_dump($query);
        $result = $this->mysqli->query($query);
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $result->close();
        return $entity;
    }
    
    public function insert(
            $moviles=-1, $commerce=-1, $street=-1){
        $query = "INSERT INTO configuracionestiempos 
                    (moviles, commerce, street) 
                VALUES 
                    ($moviles, $commerce, $street);";
        $stmt = $this->mysqli->prepare($query);
        $r = $stmt->execute();
        $stmt->close();
        return $r;
    }
    
    public function update($id, 
            $moviles=-1, $commerce=-1, $street=-1) {
        $query = "UPDATE configuracionestiempos SET 
                moviles = $moviles, commerce = $commerce, street = $street 
            WHERE id = $id;";
        if($this->checkIntID(self::TABLE, $id)){
            $stmt = $this->mysqli->prepare($query);
            $r = $stmt->execute(); 
            $stmt->close();
            return $r;
        }
        return false;
    }
	
    public function delete($id=0) {
        $stmt = $this->mysqli->prepare("DELETE FROM ". self::TABLE ." WHERE id = $id;");
        $r = $stmt->execute(); 
        $stmt->close();
        return $r;
    }
}
