<?php
/**
 * Description of RolDB
 *
 * @author WebDev
 */
class RolDB extends EntityDB {
    protected $mysqli;
    const TABLE = 'roles';
    
    public function getById($id=0){
        $stmt = $this->mysqli->prepare("SELECT * FROM roles WHERE id=?;");
        $stmt->bind_param('i', $id);
        $stmt->execute();
        $result = $stmt->get_result();
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $stmt->close();
        return $entity;
    }
    
    public function getList(){
        $result = $this->mysqli->query('SELECT * FROM roles');
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $result->close();
        return $entity;
    }
    
    public function insert($descripcion=''){
        $stmt = $this->mysqli->prepare(
                "INSERT INTO roles (descripcion) VALUES (?);");
        $stmt->bind_param('s', $descripcion);
        $r = $stmt->execute();
        $stmt->close();
        return $r;
    }
    public function delete($id=0) {
        $stmt = $this->mysqli->prepare("DELETE FROM ". self::TABLE ." WHERE id = ?;");
        $stmt->bind_param('i', $id);
        $r = $stmt->execute(); 
        $stmt->close();
        return $r;
    }  
    public function update($id, $n_descripcion) {
        if($this->checkID($id)){
            $stmt = $this->mysqli->prepare(
                    "UPDATE roles SET descripcion=? WHERE id = ?;");
            $stmt->bind_param('si',$n_descripcion, $id);
            $r = $stmt->execute(); 
            $stmt->close();
            return $r;    
        }
        return false;
    }
    
     public function checkID($id){
        $stmt = $this->mysqli->prepare("SELECT * FROM roles WHERE ID=?");
        $stmt->bind_param("i", $id);
        if($stmt->execute()){
            $stmt->store_result();    
            if ($stmt->num_rows == 1){                
                return true;
            }
        }        
        return false;
    }
}
