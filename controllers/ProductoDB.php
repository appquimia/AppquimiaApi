<?php
/**
 * Description of ProductoDB
 *
 * @author meza
 */
class ProductoDB extends EntityDB {
   protected $mysqli;
   const TABLE = 'productos';
    
    public function getById($id=0){
        $query = "SELECT * FROM " . self::TABLE . " WHERE id=$id;";
//        var_dump($query);
        $stmt = $this->mysqli->prepare($query);
        $stmt->execute();
        $result = $stmt->get_result();
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $stmt->close();
        return $entity;
    }
    
    public function getListAll(){
        $result = $this->mysqli->query(
                "SELECT p.id, p.idsponsor, s.razonsocial AS sponsor, p.producto, 
                p.descripcion, p.imagen 
            FROM productos p 
            LEFT JOIN sponsors s ON p.idsponsor = s.id");
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $result->close();
        return $entity;
    }
    
     public function getByIdSponsor($idsponsor){
        $query="SELECT p.id, p.idsponsor, s.razonsocial AS sponsor, p.producto, 
                p.descripcion, p.imagen 
            FROM productos p 
            LEFT JOIN sponsors s ON p.idsponsor = s.id
            WHERE p.idsponsor = $idsponsor";
        //var_dump($query);
        $result = $this->mysqli->query($query);
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $result->close();
        return $entity;
    }
    
    public function getList(){
        $query = "SELECT p.id, p.idsponsor, s.razonsocial AS sponsor, p.producto, 
                p.descripcion, p.imagen 
            FROM productos p 
            LEFT JOIN sponsors s ON p.idsponsor = s.id";
        $result = $this->mysqli->query($query);
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $result->close();
        return $entity;
    }
    
    public function insert(
            $idsponsor=-1, $producto='', $descripcion='', 
            $imagen=''){
        $id = $this->gen_uuid();
        $query= "INSERT INTO productos 
                (id, idsponsor, producto, 
                descripcion, imagen, fecultmodif) 
            VALUES 
                ('$id', $idsponsor, '$producto', 
                '$descripcion', '$imagen', NOW());";
//        var_dump($query);
        $stmt = $this->mysqli->prepare($query);
        $r = $stmt->execute();
        
        $stmt->close();
        return $r;
    }
    
    public function update($id='', 
            $idsponsor=-1, $producto='', $descripcion='', 
            $imagen='') {
        if($this->checkStringID(self::TABLE, $id)){
            $query = "UPDATE productos SET 
                    idsponsor=$idsponsor, producto='$producto', descripcion='$descripcion', 
                    imagen='$imagen', fecultmodif= NOW() 
                    WHERE id = '$id';";
            
            $stmt = $this->mysqli->prepare($query);
//            var_dump($query);
            $r = $stmt->execute(); 
            $stmt->close();
            return $r;
        }
        return false;
    }
    
    public function delete($id='') {
        $stmt = $this->mysqli->prepare("DELETE FROM ". self::TABLE ." WHERE id = '$id';");
        $r = $stmt->execute(); 
        $stmt->close();
        return $r;
    }
}
