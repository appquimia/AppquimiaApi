<?php
/**
 * Description of PremioAPI
 *
 * @author meza
 */
class PremioAPI extends EntityAPI {
    const API_ACTION = 'premio';
    const PREFIX_LISTALL= 'l';
    const GET_NEWS = 'n';
    
    public function API(){
        header('Content-Type: application/JSON');                
        $method = $_SERVER['REQUEST_METHOD'];
		$this->db = new PremioDB();
		
        switch ($method) {
            case 'GET'://consulta
                $this->processGet();
                break;
            case 'POST'://inserta
                $this->processPost();
                break;
            case 'PUT'://actualiza
                $this->processPut();
                break;
            case 'DELETE'://elimina
                $this->processDelete();
                break;
            default://metodo NO soportado
                echo 'METODO NO SOPORTADO';
                break;
        }
    }
    
    function processGet() {
        if (isset($_GET['id'])) {
            $id = $_GET['id'];
            $isListAll = strpos($id, self::PREFIX_LISTALL);
            $isListNew = strpos($id, self::GET_NEWS);

            if ($isListAll !== false) {
                $response = $this->db->getListAll();
                echo json_encode($response, JSON_PRETTY_PRINT);
            }elseif ($isListNew !== false) {
                $response = $this->db->getByNews($_GET['fld1']);
                echo json_encode($response, JSON_PRETTY_PRINT);
            }elseif ($id !== false) {
                $response = $this->db->getById($_GET['id']);
                echo json_encode($response, JSON_PRETTY_PRINT);
            } else {
                $this->response(400);
            }
        } else {
            $response = $this->db->getList();
            echo json_encode($response, JSON_PRETTY_PRINT);
        }
    }
    
    function processPost() {
        if($_GET['action']==self::API_ACTION)
        {
            $obj = json_decode( file_get_contents('php://input') );
            $objArr = (array)$obj;
            if (empty($objArr)) {
                $this->response(422,"error","Nothing to add. Check json");
            } else if(isset($obj->idsponsor) AND isset($obj->descripcion) AND isset($obj->direccion) 
                    AND isset($obj->fecinicio) AND isset($obj->fecfin) AND isset($obj->idpaquete) 
                    AND isset($obj->idclasificacion) AND isset($obj->imagen) AND isset($obj->fecven) 
                    AND isset($obj->codigo) ) {
                    if($this->db->insert(
                            $obj->idsponsor, $obj->descripcion, $obj->direccion,
                            $obj->fecinicio, $obj->fecfin, $obj->idpaquete, 
                            $obj->idclasificacion, $obj->imagen, $obj->fecven, 
                            $obj->codigo))
                        $this->response(200,"success","new record added");
                    else {
                        $this->response(422,"error","The property is not defined");
                    }
            } else {
                $this->response(422,"error","The property is not defined");
            } 
        }
        else
        {
            $this->response(400);
        }
    }
    
    function processPut() {
        if( isset($_GET['action']) && isset($_GET['id']) ){
            if($_GET['action']==self::API_ACTION){
                $obj = json_decode( file_get_contents('php://input') );   
                $objArr = (array)$obj;
                if (empty($objArr)){                        
                    $this->response(422,"error","Nothing to add. Check json");                        
                }else if(isset($obj->idsponsor) AND isset($obj->descripcion) AND isset($obj->direccion) 
                        AND isset($obj->fecinicio) AND isset($obj->fecfin) AND isset($obj->idpaquete) 
                        AND isset($obj->idclasificacion) AND isset($obj->imagen) AND isset($obj->fecven) 
                        AND isset($obj->codigo) ){
                    $r = $this->db->update($_GET['id'], 
                            $obj->idsponsor, $obj->descripcion, $obj->direccion, 
                            $obj->fecinicio, $obj->fecfin, $obj->idpaquete, 
                            $obj->idclasificacion, $obj->imagen, $obj->fecven, 
                            $obj->codigo);
                        $this->response(200,"success","Record updated");   
                }else{
                    $this->response(422,"error","The property is not defined");                        
                }
                exit;
           }
        }else
            $this->response(400);
    }
    
    function processDelete() {
        if (isset($_GET['action']) && isset($_GET['id'])) {
            if ($_GET['action'] == self::API_ACTION) {
                $this->db->delete($_GET['id']);
                $this->response(204);
                exit;
            }
        }
        $this->response(400);
    }
    }