<?php
/**
 * Description of TiempoEsperaDB
 *
 * @author meza
 */
class TiempoEsperaDB extends EntityDB {
   protected $mysqli;
   const TABLE = 'tiemposesperas';
   
    public function getById($id=0){
        $stmt = $this->mysqli->prepare("SELECT * FROM " 
                . self::TABLE . " WHERE id=$id;");
        $stmt->execute();
        $result = $stmt->get_result();
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $stmt->close();
        return $entity;
    }
    
    public function getList(){
        $result = $this->mysqli->query(
                "SELECT * FROM " . self::TABLE);
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $result->close();
        return $entity;
    }
    
    public function insert($costo=-1){
        $stmt = $this->mysqli->prepare(
                "INSERT INTO " . self::TABLE . " (costo) "
                . "VALUES ($costo);");
        $r = $stmt->execute();
        
        $stmt->close();
        return $r;
    }
    
    public function update($id=-1, $costo=-1) {
        if($this->checkIntID(self::TABLE, $id)){
            $stmt = $this->mysqli->prepare(
                    "UPDATE " . self::TABLE . " SET costo=$costo WHERE id = $id;");
            $r = $stmt->execute(); 
            $stmt->close();
            return $r;
        }
        return false;
    }
    
    public function delete($id=0) {
        $stmt = $this->mysqli->prepare("DELETE FROM ". self::TABLE ." WHERE id = $id;");
        $r = $stmt->execute(); 
        $stmt->close();
        return $r;
    }
}
