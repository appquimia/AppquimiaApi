<?php
/**
 * Description of TarifaPlantillaDB
 *
 * @author meza
 */
class TarifaPlantillaDB extends EntityDB {
   protected $mysqli;
   const TABLE = '';
    
    public function getTarifas(){
        $query = "SELECT id, fecha, valor, kms FROM tmptarifas;";
        //var_dump($query);
        $result = $this->mysqli->query($query);
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $result->close();
        return $entity;
    }
    
    public function getFijas(){
        $query = "SELECT id, fecha, destino, precio FROM tmptarifasfijas;";
        //var_dump($query);
        $result = $this->mysqli->query($query);
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $result->close();
        return $entity;
    }
    
    public function getEspera(){
        $query = "SELECT id, costo FROM tmptiemposesperas;";
        //var_dump($query);
        $result = $this->mysqli->query($query);
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $result->close();
        return $entity;
    }
    
    public function updateTarifa($id = -1, $valor = -1) {
        if($this->checkIntID('tmptarifas', $id)){
            $query="UPDATE tmptarifas SET 
                    fecha = NOW(),
                    valor = $valor 
                WHERE id = $id;";
//            var_dump($query);
//            return $id;
            $stmt = $this->mysqli->prepare($query);
            $r = $stmt->execute(); 
            $stmt->close();
            return $id;
        }
        return false;
    }
    
    public function updateFija($id = -1, $precio = -1) {
        if($this->checkIntID('tmptarifasfijas', $id)){
            $query="UPDATE tmptarifasfijas SET 
                    fecha = NOW(),
                    precio = $precio 
                WHERE id = $id;";
//            var_dump($query);
//            return $id;
            $stmt = $this->mysqli->prepare($query);
            $r = $stmt->execute(); 
            $stmt->close();
            return $id;
        }
        return false;
    }
    
    public function updateEspera($id = -1, $costo = -1) {
        if($this->checkIntID('tmptiemposesperas', $id)){
            $query="UPDATE tmptiemposesperas SET 
                    costo = $costo 
                WHERE id = $id;";
//            var_dump($query);
//            return $id;
            $stmt = $this->mysqli->prepare($query);
            $r = $stmt->execute(); 
            $stmt->close();
            return $id;
        }
        return false;
    }
    
    public function aplicar() {
        $stmt = $this->mysqli->prepare("SET @p_result:= -1");
        $stmt->execute();
        
        $query = "CALL sp_aplicarplantilla(@p_result);";
//        var_dump($query);
        $r = $this->mysqli->query($query);
        
        $r = $this->mysqli->query('SELECT @p_result as p_result');
        $row = $r->fetch_assoc();
        return $row['p_result'];
    }
}
