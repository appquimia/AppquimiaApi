<?php
/**
 * Description of EventoAPI
 *
 * @author meza
 */
class EventoAPI extends EntityAPI {
    const API_ACTION = 'evento';
    const PREFIX_RECLAMOS = 'r';
    
    public function API(){
        header('Content-Type: application/JSON');                
        $method = $_SERVER['REQUEST_METHOD'];
		$this->db = new EventoDB();
		
        switch ($method) {
            case 'GET'://consulta
                $this->processGet();
                break;
            case 'POST'://inserta
                $this->processPost();
                break;
            case 'PUT'://actualiza
                $this->processPut();
                break;
            case 'DELETE'://elimina
                $this->processDelete();
                break;
            case 'PATCH'://metodos de procesos
                $this->processPatch();
                break;
            default://metodo NO soportado
                echo 'METODO NO SOPORTADO';
                break;
        }
    }
    
   function processGet() {
        if ($_GET['action'] == self::API_ACTION) {
            if (isset($_GET['id'])) {
                $id = $_GET['id'];
                
                $isReclamo = (strpos($id, self::PREFIX_RECLAMOS) === 0) ? (strlen($id) > 36) : false;
                
                if ($isReclamo !== false) {
                    $response = $this->db->getEventoByIdReclamo(substr($id, 1));
                    echo json_encode($response, JSON_PRETTY_PRINT);
                } elseif ($id !== false){
                    $response = $this->db->getById($id);
                    echo json_encode($response, JSON_PRETTY_PRINT);
                }
            } else {
                $this->response(400);
            }
        } else {
            $this->response(400);
        }
    }

    function processPost() {
        if($_GET['action']==self::API_ACTION)
        {
            $obj = json_decode( file_get_contents('php://input') );
            $objArr = (array)$obj;
            if (empty($objArr)) {
                $this->response(422,"error","Nothing to add. Check json");
            } else if(isset($obj->idreclamo) AND isset($obj->descripcion) 
                    AND isset($obj->idusuario) 
                    AND isset($obj->idestadoreclamo)) {
                $r = $this->db->insert($obj->idreclamo, $obj->descripcion, 
                        $obj->idusuario, $obj->idestadoreclamo);
                
                if($r !== '-1')
                    $this->response(200,"success","new record added");
                else {
                    $this->response(422,"error","No se puede insertar");
                }
            } else {
                $this->response(422,"error","The property is not defined");
            } 
        }
        else
        {
            $this->response(400);
        }
    }
    
    function processPut() {
        if( isset($_GET['action']) && isset($_GET['id']) ){
            if($_GET['action']==self::API_ACTION){
                $obj = json_decode( file_get_contents('php://input') );   
                $objArr = (array)$obj;
                if (empty($objArr)){                        
                    $this->response(422,"error","Nothing to add. Check json");                        
                }else if(isset($obj->idreclamo) AND isset($obj->descripcion) 
                        AND isset($obj->idusuario) AND isset($obj->fechayhora) 
                        AND isset($obj->idestadoreclamo)){
                    if($this->db->update($_GET['id'], $obj->idreclamo, $obj->descripcion, 
                        $obj->idusuario, $obj->fechayhora, $obj->idestadoreclamo))
                        $this->response(200,"success","Record updated");                             
                    else
                        $this->response(304,"success","Record not updated");                             
                }else{
                    $this->response(422,"error","The property is not defined");                        
                }     
                exit;
           }
        }
        $this->response(400);
    }

    function processPatch() {
        if ($_GET['action'] == self::API_ACTION) {
            $obj = json_decode(file_get_contents('php://input'));
            $objArr = (array) $obj;
            
            if (isset($obj->id)) {
                $response = $this->db->getEventoByIdReclamo($obj->id);
                echo json_encode($response, JSON_PRETTY_PRINT);
            } else {
                $this->response(400);
            }
        }
    }
    
    function processDelete() {
        if (isset($_GET['action']) && isset($_GET['id'])) {
            if ($_GET['action'] == self::API_ACTION) {
                $this->db->delete($_GET['id']);
                $this->response(204);
                exit;
            }
        }
        $this->response(400);
    }
    }
