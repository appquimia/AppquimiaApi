<?php
/**
 * Description of ReclamoDB
 *
 * @author meza
 */
class ReclamoDB extends EntityDB {
   protected $mysqli;
   const TABLE = 'reclamos';
    
    public function getById($id=''){
        $query = "SELECT r.id,r.idpremio, r.idcuenta, r.idestado, e.estado, "
                . "r.fecha, p.idsponsor, s.razonsocial, p.descripcion, c.nombre "
                . "FROM reclamos r "
                . "LEFT JOIN reclamosestados e ON r.idestado = e.id "
                . "LEFT JOIN premios p ON r.idpremio = p.id "
                . "LEFT JOIN sponsors s ON p.idsponsor = s.id "
                . "LEFT JOIN cuentas c ON r.idcuenta = c.id "
                . "WHERE r.id = '" . $id . "'";
        //var_dump($query);
        $stmt = $this->mysqli->prepare($query);
        $stmt->execute();
        $result = $stmt->get_result();
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $stmt->close();
        return $entity;
    }
    
    public function getReclamosByIdEstado($id=1){
        $query = "SELECT r.id,r.idpremio, r.idcuenta, r.idestado, e.estado, "
                . "r.fecha, p.idsponsor, s.razonsocial, p.descripcion, c.nombre "
                . "FROM reclamos r "
                . "LEFT JOIN reclamosestados e ON r.idestado = e.id "
                . "LEFT JOIN premios p ON r.idpremio = p.id "
                . "LEFT JOIN sponsors s ON p.idsponsor = s.id "
                . "LEFT JOIN cuentas c ON r.idcuenta = c.id "
                . "WHERE e.id = " . $id;
        
        //var_dump($query);
        $result = $this->mysqli->query($query);
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $result->close();
        return $entity;
    }
    
   public function getListReclamosByIdSponsors($id=1){
        
       $query = "SELECT r.id, r.idpremio, r.idcuenta, r.idestado, e.estado, "
               . "r.fecha, p.idsponsor, s.razonsocial, p.descripcion, c.nombre "
                . "FROM reclamos r "
                . "LEFT JOIN reclamosestados e ON r.id = e.id "
                . "LEFT JOIN premios p ON r.idpremio = p.id "
                . "LEFT JOIN sponsors s ON p.idsponsor = s.id "
                . "LEFT JOIN cuentas c ON r.idcuenta = c.id "
                . "WHERE s.id = " . $id;
       $result = $this->mysqli->query($query);
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $result->close();
        return $entity;
    }
    
   public function getResumen(){
        
       $query = "SELECT IFNULL(COUNT(r.id), '0') AS total, "
               . "IFNULL(CASE e.id WHEN 1 THEN COUNT(r.id) END, '0') AS pendientes, "
               . "IFNULL(CASE e.id WHEN 2 THEN COUNT(r.id) END, '0') AS atendidos, "
               . "IFNULL(CASE e.id WHEN 3 THEN COUNT(r.id) END, '0') AS cerrados "
               . "FROM reclamosestados e "
               . "LEFT JOIN reclamos r ON r.idestado = e.id";
       $result = $this->mysqli->query($query);
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $result->close();
        return $entity;
    }
    
    /*
     * Sólo se usa desde app móvil
     */
    public function insert($id= '', $idpremio=-1, $idcuenta='', 
        $idestado=-1, $fecha='', $hora=''){
        $stmt = $this->mysqli->prepare(
                "INSERT INTO " . self::TABLE . " (id, idpremio, idcuenta, "
                . "idestado, fecha, hora) "
                . "VALUES (?, ?, ?, ?, ?, ?);");
        $stmt->bind_param('sisiss', $id, $idpremio, $idcuenta, $idestado, $fecha, $hora);
        $r = $stmt->execute();
        $stmt->close();
        return $r;
    }
    public function delete($id=0) {
        $stmt = $this->mysqli->prepare("DELETE FROM ". self::TABLE ." WHERE id = ?;");
        $stmt->bind_param('i', $id);
        $r = $stmt->execute(); 
        $stmt->close();
        return $r;
    }
    public function update($id=-1, $idpremio=-1, $idcuenta=-1, 
            $idestado=-1, $fecha='', $hora='') {
        if($this->checkID($id)){
            $stmt = $this->mysqli->prepare(
                    "UPDATE " . self::TABLE . " SET idpremio=?, idcuenta=?, "
                    . "idestado=?, fecha=?, hora=? "
                    . "WHERE id = ?;");
            $stmt->bind_param('iiissi', $idpremio, $idcuenta, $idestado, $fecha, $hora, $id);
            $r = $stmt->execute(); 
            $stmt->close();
            return $r;
        }
        return false;
    }
    
     public function checkID($id){
        $stmt = $this->mysqli->prepare("SELECT * FROM " . self::TABLE 
                . " WHERE ID=?");
        $stmt->bind_param("i", $id);
        if($stmt->execute()){
            $stmt->store_result();    
            if ($stmt->num_rows == 1){                
                return true;
            }
        }        
        return false;
    }
}

