<?php
/**
 * Description of TarifasFijasAPI
 *
 * @author meza
 */
class TiempoEsperaAPI extends EntityAPI {
    const API_ACTION = 'tiempoespera';
    
    public function API(){
        header('Content-Type: application/JSON');                
        $method = $_SERVER['REQUEST_METHOD'];
		$this->db = new TiempoEsperaDB();
		
        switch ($method) {
            case 'GET'://consulta
                $this->processGet();
                break;
            case 'POST'://inserta
                $this->processPost();
                break;
            case 'PUT'://actualiza
                $this->processPut();
                break;
            case 'DELETE'://elimina
                $this->proceesDelete();
                break;
            default://metodo NO soportado
                echo 'METODO NO SOPORTADO';
                break;
        }
    }
    
    function processGet(){
        $id = filter_input(INPUT_GET, 'id');
        if($id){
            $response = $this->db->getById($id);
            echo json_encode($response,JSON_PRETTY_PRINT);
        }else{
            $response = $this->db->getList();
            echo json_encode($response,JSON_PRETTY_PRINT);
        }
    }
    
    function processPost() {
        $obj = json_decode( file_get_contents('php://input') );
        $objArr = (array)$obj;
        if (empty($objArr)) {
            $this->response(422,"error","Nothing to add. Check json");
        } else if(isset($obj->costo)) {
            $r = $this->db->insert($obj->costo);
            if($r)
                $this->response(200,"success","new record added");
        } else {
            $this->response(422,"error","The property is not defined");
        }
    }
    
    function processPut() {        
        $id = filter_input(INPUT_GET, 'id');
        if (!$id) {
            $this->response(400);
            exit;
        }
        
        $obj = json_decode( file_get_contents('php://input') );   
        $objArr = (array)$obj;
        if (empty($objArr)){
            $this->response(422,"error","Nothing to add. Check json");                        
        } else if(isset($obj->costo)) {
            $r = $this->db->update($id, $obj->costo);
            return ($r) ? ($this->response(200,"success","Record updated")) : 
                ($this->response(304,"success","Record not updated"));
        } else {
            $this->response(422,"error","The property is not defined");                        
        }
    }
    
    function proceesDelete() {
        $id = filter_input(INPUT_GET, 'id');
        if ($id) {
            $this->db->delete($id);
            $this->response(204);
            exit;
        }
        $this->response(400);
    }
}
