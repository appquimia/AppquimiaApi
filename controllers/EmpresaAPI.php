<?php
/**
 * Description of EmpresaAPI
 *
 * @author meza
 */
class EmpresaAPI extends EntityAPI {
    const API_ACTION = 'empresa';
	
     public function API(){
        header('Content-Type: application/JSON');    
        $method = $_SERVER['REQUEST_METHOD'];
		$this->db = new EmpresaDB();
		
        switch ($method) {
            case 'GET'://consulta
                $this->processGet();
                break;
            case 'POST'://inserta
                $this->processPost();
                break;
            case 'PUT'://actualiza
                $this->processPut();
                break;
            case 'DELETE'://elimina
                $this->processDelete();
                break;
            default://metodo NO soportado
                echo 'METODO NO SOPORTADO';
                break;
        }
    }
	
      function processGet(){
        if($_GET['action']==self::API_ACTION){
            if(isset($_GET['id'])){
                $response = $this->db->getById($_GET['id']);
                echo json_encode($response,JSON_PRETTY_PRINT);
            }else{
                $response = $this->db->getList();
                echo json_encode($response,JSON_PRETTY_PRINT);
            }
        }else{
                $this->response(400);
        }
    } 
	
    function processPost() {
        if($_GET['action']==self::API_ACTION)
        {
            $obj = json_decode( file_get_contents('php://input') );
            $objArr = (array)$obj;
            if (empty($objArr)) {
                $this->response(422,"error","Nothing to add. Check json");
            } else if(isset($obj->empresa)) {
                $r = $this->db->insert($obj->empresa);
                if($r)
                    $this->response(200,"success","new record added");
            } else {
                $this->response(422,"error","The property is not defined");
            } 
        }
		else
        {
            $this->response(400);
        }
    }
    
    function processPut() {
        if( isset($_GET['action']) && isset($_GET['id']) ){
            if($_GET['action']==self::API_ACTION){
                $obj = json_decode( file_get_contents('php://input') );   
                $objArr = (array)$obj;
                if (empty($objArr)){                        
                    $this->response(422,"error","Nothing to add. Check json");                        
                }else if(isset($obj->empresa)) {
                    if($this->db->update($_GET['id'], $obj->empresa))
                        $this->response(200,"success","Record updated");                             
                    else
                        $this->response(304,"success","Record not updated");                             
                }else{
                    $this->response(422,"error","The property is not defined");                        
                }     
                exit;
           }
        }
        $this->response(400);
    }
    function processDelete() {
        if (isset($_GET['action']) && isset($_GET['id'])) {
            if ($_GET['action'] == self::API_ACTION) {
                $this->db->delete($_GET['id']);
                $this->response(204);
                exit;
            }
        }
        $this->response(400);
    }
    }
