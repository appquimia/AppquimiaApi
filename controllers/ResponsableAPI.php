<?php
/**
 * Description of ResponsalbeAPI
 *
 * @author meza
 */
class ResponsableAPI extends EntityAPI {
    const API_ACTION = 'responsable';
    
    public function API(){
        header('Content-Type: application/JSON');                
        $method = $_SERVER['REQUEST_METHOD'];
		$this->db = new ResponsableDB();
		
        switch ($method) {
            case 'GET'://consulta
                $this->processGet();
                break;
            default://metodo NO soportado
                echo 'METODO NO SOPORTADO';
                break;
        }
    }
    
    function processGet(){
        if($_GET['action']==self::API_ACTION){
            if(isset($_GET['id'])){
                $response = $this->db->getById($_GET['id']);
                echo json_encode($response,JSON_PRETTY_PRINT);
            }else{
                $response = $this->db->getList();
                echo json_encode($response,JSON_PRETTY_PRINT);
            }
        }else{
                $this->response(400);
        }
    }
}
