<?php
/**
 * Description of PublicidadApi
 *
 * @author meza
 */
class PublicidadAPI extends EntityAPI {
    const API_ACTION = 'publicidad';
    const GET_NEWS = 'n';
    
    public function API(){
        header('Content-Type: application/JSON');                
        $method = $_SERVER['REQUEST_METHOD'];
		$this->db = new PublicidadDB();
		
        switch ($method) {
            case 'GET'://consulta
                $this->processGet();
                break;
            case 'POST'://inserta
                $this->processPost();
                break;
            case 'PUT'://actualiza
                $this->processPut();
                break;
            case 'DELETE'://elimina
                $this->processDelete();
                break;
            default://metodo NO soportado
                echo 'METODO NO SOPORTADO';
                break;
        }
    }
    
    function processGet() {
        if (isset($_GET['id'])) {
            $id = $_GET['id'];
            $isListNew = strpos($id, self::GET_NEWS);

            if ($isListNew !== false) {
                $response = $this->db->getByNews($_GET['fld1']);
                echo json_encode($response, JSON_PRETTY_PRINT);
            }elseif ($id !== false) {
                $response = $this->db->getById($_GET['id']);
                echo json_encode($response, JSON_PRETTY_PRINT);
            } else {
                $this->response(400);
            }
        } else {
            $response = $this->db->getList();
            echo json_encode($response, JSON_PRETTY_PRINT);
        }
    }
    
    function processPost() {
        if($_GET['action']==self::API_ACTION)
        {
            $obj = json_decode( file_get_contents('php://input') );
            $objArr = (array)$obj;
            if (empty($objArr)) {
                $this->response(422,"error","Nothing to add. Check json");
            } else if(isset($obj->publicidad) AND isset($obj->url) 
                    AND isset($obj->fecinicio) AND isset($obj->fecfin) 
                    AND isset($obj->idclasificacion)) {
                $r = $this->db->insert($obj->publicidad, $obj->url, 
                        $obj->fecinicio, $obj->fecfin, 
                        $obj->idclasificacion);
                if($r)
                    $this->response(200,"success","new record added");
            } else {
                $this->response(422,"error","The property is not defined");
            } 
        }
        else
        {
            $this->response(400);
        }
    }
    
    function processPut() {
        if( isset($_GET['action']) && isset($_GET['id']) ){
            if($_GET['action']==self::API_ACTION){
                $obj = json_decode( file_get_contents('php://input') );   
                $objArr = (array)$obj;
                if (empty($objArr)){                        
                    $this->response(422,"error","Nothing to add. Check json");                        
                }else if(isset($obj->publicidad) AND isset($obj->url) 
                        AND isset($obj->fecinicio) AND isset($obj->fecfin) 
                         AND isset($obj->idclasificacion)){
                    if($this->db->update($_GET['id'], 
                        $obj->publicidad, $obj->url, 
                        $obj->fecinicio, $obj->fecfin, 
                        $obj->idclasificacion))
                        $this->response(200,"success","Record updated");                             
                    else
                        $this->response(304,"success","Record not updated");                             
                }else{
                    $this->response(422,"error","The property is not defined");                        
                }     
                exit;
           }
        }
        $this->response(400);
    }
    function processDelete() {
        if (isset($_GET['action']) && isset($_GET['id'])) {
            if ($_GET['action'] == self::API_ACTION) {
                $this->db->delete($_GET['id']);
                $this->response(204);
                exit;
            }
        }
        $this->response(400);
    }
 }