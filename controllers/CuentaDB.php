<?php
/**
 * Description of CuentaDB
 *
 * @author meza
 */
class CuentaDB extends EntityDB {
   protected $mysqli;
   const TABLE = 'cuentas';
    
    public function getById($id=0){
        $stmt = $this->mysqli->prepare("SELECT * FROM " 
                . self::TABLE . " WHERE id=?;");
        $stmt->bind_param('s', $id);
        $stmt->execute();
        $result = $stmt->get_result();
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $stmt->close();
        return $entity;
    }
    
    public function getList(){
        $result = $this->mysqli->query(
                "SELECT c.id, c.nombre, c.email, c.imagen, c.provider, c.celular "
                . "FROM cuentas c ");
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $result->close();
        return $entity;
    }
    
    public function insert($id='', $nombre='', $email='', $imagen='', $provider='', $celular=''){
        if(!$this->checkID($id)){
            $stmt = $this->mysqli->prepare(
                    "INSERT INTO " . self::TABLE . " (id, nombre, email, imagen, provider, celular)"
                    . "VALUES (?, ?, ?, ?, ?, ?);");
            $stmt->bind_param('ssssss', $id, $nombre, $email, $imagen, $provider, $celular);
            $r = $stmt->execute();

            $stmt->close();        
        } else {
            $r = $this->update($id='', $nombre='', $email='', $imagen='', $provider='', $celular='');
        }
        
        return $r;
    }
    public function delete($id=0) {
        $stmt = $this->mysqli->prepare("DELETE FROM ". self::TABLE ." WHERE id = ?;");
        $stmt->bind_param('i', $id);
        $r = $stmt->execute(); 
        $stmt->close();
        return $r;
    }
    public function update($id='', $nombre='', $email='', $imagen='', $provider='', $celular='') {
        if($this->checkID($id)){
            $stmt = $this->mysqli->prepare(
                    "UPDATE " . self::TABLE . " SET nombre=?, email=?, imagen=?, provider=?, celular=? "
                    . "WHERE id = ?;");
            $stmt->bind_param('ssssss', $nombre, $email, $imagen, $provider, $celular, $id);
            $r = $stmt->execute(); 
            $stmt->close();
            return $r;
        }
        return false;
    }
    
     public function checkID($id){
        $stmt = $this->mysqli->prepare("SELECT * FROM " . self::TABLE 
                . " WHERE id='" . $id . "'");
        
        if($stmt->execute()){
            $stmt->store_result();    
            if ($stmt->num_rows == 1){                
                return true;
            }
        }        
        return false;
    }
}
