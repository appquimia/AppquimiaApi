<?php
/**
 * Description of EntityDB
 *
 * @author meza
 */
abstract class EntityDB {
    public function __construct() {
        $ini = parse_ini_file('conf.ini', true);
       
        try{
            //conexión a base de datos
            if($ini['enviroment']['prod']==1) {
                $conf=$ini['prod'];
            } else {
                $conf=$ini['debug'];
            }
            $this->mysqli = new mysqli(
                    $conf['server'] , $conf['user'], $conf['password'], 
                    $conf['database'], $conf['port']);
            mysqli_set_charset($this->mysqli, "utf8");
        }catch (mysqli_sql_exception $e){
            //Si no se puede realizar la conexión
            http_response_code(500);
            exit;
        }
    }
    
    public function checkIntID($table, $id){
        $stmt = $this->mysqli->prepare("SELECT * FROM $table WHERE id=$id");
        if($stmt->execute()){
            $stmt->store_result();    
            if ($stmt->num_rows == 1){                
                return true;
            }
        }        
        return false;
    }
    
    public function checkStringID($table, $id){
        $stmt = $this->mysqli->prepare("SELECT * FROM $table WHERE id='$id'");
        if($stmt->execute()){
            $stmt->store_result();    
            if ($stmt->num_rows == 1){                
                return true;
            }
        }        
        return false;
    }
    
    protected function gen_uuid() {
        return sprintf( '%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
            // 32 bits for "time_low"
            mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ),
            // 16 bits for "time_mid"
            mt_rand( 0, 0xffff ),
            // 16 bits for "time_hi_and_version",
            // four most significant bits holds version number 4
            mt_rand( 0, 0x0fff ) | 0x4000,
            // 16 bits, 8 bits for "clk_seq_hi_res",
            // 8 bits for "clk_seq_low",
            // two most significant bits holds zero and one for variant DCE1.1
            mt_rand( 0, 0x3fff ) | 0x8000,
            // 48 bits for "node"
            mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff )
    );
}
}
