<?php
/**
 * Description of BitacoraDB
 *
 * @author meza
 */
class BitacoraDB {
   protected $mysqli;
   const TABLE = 'bitacoras';
    
    public function getById($id=0){
        $stmt = $this->mysqli->prepare("SELECT * FROM " 
                . self::TABLE . " WHERE id=$id;");
        $stmt->execute();
        $result = $stmt->get_result();
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $stmt->close();
        return $entity;
    }
    
    public function getBitacoraByIdCuenta($id=1){
        $query = "SELECT b.idcuenta, c.nombre, b.idmovil, b.fecha, b.costo "
                . "FROM bitacoras b "
                . "LEFT JOIN cuentas c ON b.idcuenta = c.id "
                . "LEFT JOIN moviles m ON b.idmovil = m.id "
                . "WHERE b.idcuenta = " . $id;
        $result = $this->mysqli->query($query);
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $result->close();
        return $entity;
    }
    
    public function insert($idcuenta=-'', $idmovil=-1, 
        $fecha='', $costo=-1){
        $stmt = $this->mysqli->prepare(
                "INSERT INTO " . self::TABLE . " (idcuenta, idmovil, "
                . "fecha, costo) "
                . "VALUES (?, ?, ?, ?);");
        $stmt->bind_param('sisd', $idcuenta, $idmovil, $fecha, $costo);
        $r = $stmt->execute();
        
        $stmt->close();
        return $r;
    }
    
    public function update($id='', $idcuenta='', $idmovil=-1, 
            $fecha='', $costo=-1) {
        if($this->checkStringID(self::TABLE, $id)){
            $stmt = $this->mysqli->prepare(
                    "UPDATE " . self::TABLE . " SET idcuenta=?, idmovil=?, "
                    . "fecha=?, costo=? "
                    . "WHERE id = ?;");
            $stmt->bind_param('sisds', $idcuenta, $idmovil, $fecha, $costo, $id);
            $r = $stmt->execute(); 
            $stmt->close();
            return $r;
        }
        return false;
    }
    
    public function delete($id=0) {
        $stmt = $this->mysqli->prepare("DELETE FROM ". self::TABLE ." WHERE id = $id;");
        $r = $stmt->execute(); 
        $stmt->close();
        return $r;
    }
}
