<?php
/**
 * Description of TarifaDB
 *
 * @author meza
 */
class TarifaDB extends EntityDB {
   protected $mysqli;
   const TABLE = 'tarifas';
    
    public function getById($id=0){
        $stmt = $this->mysqli->prepare("SELECT * FROM " 
                . self::TABLE . " WHERE id=?;");
        $stmt->bind_param('i', $id);
        $stmt->execute();
        $result = $stmt->get_result();
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $stmt->close();
        return $entity;
    }
    
    public function getList(){
        $result = $this->mysqli->query(
                "SELECT t.id, t.fecha, t.valor, t.kms "
                . "FROM `tarifas` t ");
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $result->close();
        return $entity;
    }
    
    public function insert($fecha='', $valor=-1, 
        $kms=-1){
        $stmt = $this->mysqli->prepare(
                "INSERT INTO " . self::TABLE . " (fecha, valor, "
                . "kms) "
                . "VALUES (?, ?, ?);");
        $stmt->bind_param('sdd', $fecha, $valor, $kms);
        $r = $stmt->execute();
        
        $stmt->close();
        return $r;
    }
    public function delete($id=0) {
        $stmt = $this->mysqli->prepare("DELETE FROM ". self::TABLE ." WHERE id = ?;");
        $stmt->bind_param('i', $id);
        $r = $stmt->execute(); 
        $stmt->close();
        return $r;
    }
    public function update($id=-1, $fecha='', $valor=-1, 
            $kms=-1) {
        if($this->checkID($id)){
            $stmt = $this->mysqli->prepare(
                    "UPDATE " . self::TABLE . " SET fecha=?, valor=?, "
                    . "kms=?  "
                    . "WHERE id = ?;");
            $stmt->bind_param('sddi', $fecha, $valor, $kms, $id);
            $r = $stmt->execute(); 
            $stmt->close();
            return $r;
        }
        return false;
    }
    
     public function checkID($id){
        $stmt = $this->mysqli->prepare("SELECT * FROM " . self::TABLE 
                . " WHERE ID=?");
        $stmt->bind_param("i", $id);
        if($stmt->execute()){
            $stmt->store_result();    
            if ($stmt->num_rows == 1){                
                return true;
            }
        }        
        return false;
    }
}
