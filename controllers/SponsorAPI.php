<?php
/**
 * Description of SponsorAPI
 *
 * @author meza
 */
class SponsorAPI extends EntityAPI {
    const API_ACTION = 'sponsor';
    const GET_CONTACTOSXSPONSORS = 'c';
    const GET_SPONSORS = 's';
    const GET_NEWS = 'n';

    public function __construct() {
        $this->db = new SponsorDB();
        $this->fields = [];
        array_push($this->fields, 
                'razonsocial',
                'direcciones',
                'codigocanjeo',
                'telefono',
                'cuit',
                'email',
                'descripcion');
    }
    
    function processGet(){
        $id = filter_input(INPUT_GET, 'id');
        $isContactoxSponsor = isset($id) ? $id === self::GET_CONTACTOSXSPONSORS : false;
        $isSponsor = isset($id) ? $id === self::GET_SPONSORS : false;
        $isNews = isset($id) ? $id === self::GET_NEWS : false;
        
        if($isContactoxSponsor) {
            $idfld = filter_input(INPUT_GET, 'fld1');
            $response = $this->db->getContactoByIdSponsor($idfld);
            echo json_encode($response,JSON_PRETTY_PRINT);
        }elseif($isSponsor) {
//            $idfld = filter_input(INPUT_GET, 'fld1');
            $response = $this->db->getListBySponsors(); // $idfld);
            echo json_encode($response,JSON_PRETTY_PRINT);
        }elseif($isNews) {
            $idfld = filter_input(INPUT_GET, 'fld1');
            $response = $this->db->getNews($idfld);
            echo json_encode($response,JSON_PRETTY_PRINT);
        }elseif($id){
            $response = $this->db->getById($id);
            echo json_encode($response,JSON_PRETTY_PRINT);
        }else{
            $response = $this->db->getList();
            echo json_encode($response,JSON_PRETTY_PRINT);
        }
    }
    
    function processPost() {
        $obj = json_decode( file_get_contents('php://input') );
        $objArr = (array)$obj;
        if (empty($objArr)) {
            $this->response(422,"error","Nothing to add. Check json");
            exit;
        }        
        if(!$this->checkFields($obj)) {
            $this->response(422,"error","The property is not defined");
            exit;
        }
        $r = $this->db->insert(
                $obj->razonsocial, $obj->direcciones, $obj->codigocanjeo, 
                $obj->telefono, $obj->cuit, $obj->email, 
                $obj->descripcion, $obj->imagen);
        if($r) {$this->response(200,"success", $r); }
        else {$this->response(204,"error","No record added"); }
    }
    
    function processPut() {
        $obj = json_decode(file_get_contents('php://input') );
        if(!$this->checkFields($obj)) {
            $this->response(422,"error","The property is not defined");
            exit;
        }
        $id = filter_input(INPUT_GET, 'id');
        if(!$id) {
            $this->response(422,"error","Id no enviado.");
            exit;
        }
        $r = $this->db->update($id,
                $obj->razonsocial, $obj->direcciones, $obj->codigocanjeo, 
                $obj->telefono, $obj->cuit, $obj->email, 
                $obj->descripcion, $obj->imagen);
        if($r) { $this->response(200,"success", $id); }
        else { $this->response(204,"success","Record not updated");}
    }
}
