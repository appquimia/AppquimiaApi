<?php
/**
 * Description of SucursalAPI
 *
 * @author meza
 */
class SucursalAPI extends EntityAPI {
    const API_ACTION = 'sucursal';
    const GET_BYIDSPONSOR = 'byidsponsor';
    const GET_NEWS = 'n';

    public function __construct() {
        $this->db = new SucursalDB();
        $this->fields = [];
        array_push($this->fields, 
                'idsponsor',
                'sucursal',
                'direccion',
                'descripcion');
    }
    
    function processGet(){
        $id = filter_input(INPUT_GET, 'id');
        $isByIdSponsor = isset($id) ? $id === self::GET_BYIDSPONSOR : false;
        $isNews = isset($id) ? $id === self::GET_NEWS : false;
        
        if($isByIdSponsor) {
            $idfld = filter_input(INPUT_GET, 'fld1');
            $response = $this->db->getByIdSponsor($idfld);
            echo json_encode($response,JSON_PRETTY_PRINT);
        } elseif($isNews) {
            $idfld = filter_input(INPUT_GET, 'fld1');
            $response = $this->db->getNews($idfld);
            echo json_encode($response,JSON_PRETTY_PRINT);
        } elseif($id) {
            $response = $this->db->getById($id);
            echo json_encode($response,JSON_PRETTY_PRINT);
        } else {
            $response = $this->db->getList();
            echo json_encode($response,JSON_PRETTY_PRINT);
        }
    }
    
    function processPost() {
        $obj = json_decode( file_get_contents('php://input') );
        $objArr = (array)$obj;
        
        if (empty($objArr)) {
            $this->response(422,"error","Nothing to add. Check json");
            exit;
        }
        
        if(!$this->checkFields($obj)) {
            $this->response(422,"error","The property is not defined");
            exit;
        }
        
        $r = $this->db->insert(
                $obj->idsponsor, $obj->sucursal, $obj->direccion, 
                $obj->descripcion);
        if($r) {$this->response(200,"success", $r); }
        else {$this->response(204,"error","No record added"); }
    }
    
    function processPut() {
        $obj = json_decode(file_get_contents('php://input') );
        if(!$this->checkFields($obj)) {
            $this->response(422,"error","The property is not defined");
            exit;
        }
        $id = filter_input(INPUT_GET, 'id');
        if(!$id) {
            $this->response(422,"error","Id no enviado.");
            exit;
        }
        $r = $this->db->update($id,
                $obj->idsponsor, $obj->sucursal, $obj->direccion, 
                $obj->descripcion);
        if($r) { $this->response(200,"success", $id); }
        else { $this->response(204,"success","Record not updated");}
    }
}
