<?php
    require_once "./controllers/EntityAPI.php";
    require_once "./controllers/EntityDB.php";
    require_once "./controllers/UsuarioAPI.php";
    require_once "./controllers/UsuarioDB.php";
    require_once "./controllers/RolAPI.php";
    require_once "./controllers/RolDB.php";
    require_once "./controllers/MovilAPI.php";
    require_once "./controllers/MovilDB.php";
    require_once "./controllers/ConductorAPI.php";
    require_once "./controllers/ConductorDB.php";
    require_once "./controllers/TarifaAPI.php";
    require_once "./controllers/TarifaDB.php";
    require_once "./controllers/TarifaFijaAPI.php";
    require_once "./controllers/TarifaFijaDB.php";
    require_once "./controllers/SponsorAPI.php";
    require_once "./controllers/SponsorDB.php";
    require_once "./controllers/PublicidadAPI.php";
    require_once "./controllers/PublicidadDB.php";
    require_once "./controllers/PaqueteAPI.php";
    require_once "./controllers/PaqueteDB.php";
    require_once "./controllers/PremioAPI.php";
    require_once "./controllers/PremioDB.php";
    require_once "./controllers/ConfigJuegoAPI.php";
    require_once "./controllers/ConfigJuegoDB.php";
    require_once "./controllers/PremioEstadoAPI.php";
    require_once "./controllers/PremioEstadoDB.php";
    require_once "./controllers/ContactoAPI.php";
    require_once "./controllers/ContactoDB.php";
    require_once "./controllers/ResponsableAPI.php";
    require_once "./controllers/ResponsableDB.php";
    require_once "./controllers/CuentaAPI.php";
    require_once "./controllers/CuentaDB.php";
    require_once "./controllers/CalificacionAPI.php";
    require_once "./controllers/CalificacionDB.php";
    require_once "./controllers/BitacoraAPI.php";
    require_once "./controllers/BitacoraDB.php";
    require_once "./controllers/ClasificacionAPI.php";
    require_once "./controllers/ClasificacionDB.php";
    require_once "./controllers/ReclamoAPI.php";
    require_once "./controllers/ReclamoDB.php";
    require_once "./controllers/ReclamoEstadoAPI.php";
    require_once "./controllers/ReclamoEstadoDB.php";
    require_once "./controllers/EventoAPI.php";
    require_once "./controllers/EventoDB.php";
    require_once "./controllers/ConductorxMovilAPI.php";
    require_once "./controllers/ConductorxMovilDB.php";
    require_once "./controllers/EmpresaAPI.php";
    require_once "./controllers/EmpresaDB.php";
    require_once "./controllers/TiempoEsperaAPI.php";
    require_once "./controllers/TiempoEsperaDB.php";
    require_once "./controllers/PremioXConductorAPI.php";
    require_once "./controllers/PremioXConductorDB.php";
    require_once "./controllers/ConfiguracionTiempoAPI.php";
    require_once "./controllers/ConfiguracionTiempoDB.php";
    require_once "./controllers/SucursalAPI.php";
    require_once "./controllers/SucursalDB.php";
    require_once "./controllers/ProductoAPI.php";
    require_once "./controllers/ProductoDB.php";
    require_once "./controllers/TarifaPlantillaAPI.php";
    require_once "./controllers/TarifaPlantillaDB.php";
    require_once "./controllers/VersionAPI.php";
    require_once "./controllers/VersionDB.php";
        
    header("Access-Control-Allow-Origin: *");
    header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS, PATCH');
    header('Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept, Cache-Control, Pragma');
    
    $action = $_GET['action'];
    $api = NULL;
    
    switch($action)
    {
        case UsuarioAPI::API_ACTION:
            $api = new UsuarioAPI();
            break;
        case RolAPI::API_ACTION:
            $api = new RolAPI();
            break;
        case MovilAPI::API_ACTION:
            $api = new MovilAPI();
            break;
        case ConductorAPI::API_ACTION:
            $api = new ConductorAPI();
            break;
        case TarifaAPI::API_ACTION:
            $api = new TarifaAPI();
            break;
        case TarifaFijaAPI::API_ACTION:
            $api = new TarifaFijaAPI();
            break;
        case SponsorAPI::API_ACTION:
            $api = new SponsorAPI();
            break;
        case PublicidadAPI::API_ACTION:
            $api = new PublicidadAPI();
            break;
        case PaqueteAPI::API_ACTION:
            $api = new PaqueteAPI();
            break;
        case PremioAPI::API_ACTION:
            $api = new PremioAPI();
            break;
        case ConfigJuegoAPI::API_ACTION:
            $api = new ConfigJuegoAPI();
            break;
        case PremioEstadoAPI::API_ACTION:
            $api = new PremioEstadoAPI();
            break;
        case ContactoAPI::API_ACTION:
            $api = new ContactoAPI();
            break;
        case ResponsableAPI::API_ACTION:
            $api = new ResponsableAPI();
            break;
        case CuentaAPI::API_ACTION:
            $api = new CuentaAPI();
            break;
        case CalificacionAPI::API_ACTION:
            $api = new CalificacionAPI();
            break;
        case BitacoraAPI::API_ACTION:
            $api = new BitacoraAPI();
            break;
        case ClasificacionAPI::API_ACTION:
            $api = new ClasificacionAPI();
            break;
        case ReclamoAPI::API_ACTION:
            $api = new ReclamoAPI();
            break;
        case ReclamoEstadoAPI::API_ACTION:
            $api = new ReclamoEstadoAPI();
            break;
        case EventoAPI::API_ACTION:
            $api = new EventoAPI();
            break;
        case ConductorxMovilAPI::API_ACTION:
            $api = new ConductorxMovilAPI();
            break;
        case EmpresaAPI::API_ACTION:
            $api = new EmpresaAPI();
            break;
        case TiempoEsperaAPI::API_ACTION:
            $api = new TiempoEsperaAPI();
            break;
        case PremioXConductorAPI::API_ACTION:
            $api = new PremioXConductorAPI();
            break;
        case ConfiguracionTiempoAPI::API_ACTION:
            $api = new ConfiguracionTiempoAPI();
            break;
        case SucursalAPI::API_ACTION:
            $api = new SucursalAPI();
            break;
        case ProductoAPI::API_ACTION:
            $api = new ProductoAPI();
            break;
        case TarifaPlantillaAPI::API_ACTION:
            $api = new TarifaPlantillaAPI();
            break;
        case VersionAPI::API_ACTION:
            $api = new VersionAPI();
            break;
        default:
            echo 'METODO NO SOPORTADO';
            break;        
    }
    
    if($api != NULL) {
        $api->API();
    }
